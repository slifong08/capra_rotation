#!/bin/bash/python

# The purpose of this script is to merge the resulting multiintersected roadmap datasets together. 

#on 2018-01-22, enhancers with H3K27ac peaks not overlapping H3K4me3 peaks were identified for each of the 98 roadmap datasets. Those datasets were then multi-intersected together using the script "roadmap_find_common_enhancer.py"

# a note on multiintersecting. The multiintersect function does not consider the -f 0.5 -r command when interesecting files together. It will simply multiintersect all of the peaks and classify the fragments by how much overlap was found for each fragment. This generates lots of individual fragment records that only map to one tissue dataset and may only be a couple base pairs in length. For this, it is important to merge the fragments together to gain an idea of fragment overlap between different datasets. 

import os

dates = ["2018-01-22", "2018-01-23"]

path = ("/home/fongsl/roadmap/%s" % i for i in dates)

for i in path:
    os.chdir(i)
    print(i)
    cmd= "mergeBed -i all_tissue_Hsap_H3K27ac_plus_H3K4me3_minus_no_names.bed -d 100 -c 4 -o collapse -delim ','> merged_no_name.bed"
    print(cmd)
    os.system(cmd)
