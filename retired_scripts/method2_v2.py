#!/bin/bash/python

#180219 METHOD2 V2 Random sampling of Roadmap datasets to characterize background levels of private enhancer activity across Roadmap tissues. This is a control experiment to evaluate if the 4% of private enhancer activity observed in Villar and not in any other Roadmap tissue dataset is a reasonable frequency for any private enhancer. 

#180216 "High Quality" (HQ) villar species ChIP-SEQ datasets were separately analyzed versus Broadly Active Enhancers (BAEs) overlapping the 90th% of enhancers identified in Method 2. We have found that most BAEs do not contain any sequence that aligns with other species, suggesting these enhancers correspond to human specific sequences in regions of the genome recently acquired. To make sure that our finding is not an artifact of noisy sequencing datasets,  we limited our comparison to the high_quality_species datasets.

#180210 METHOD 2 V1 - This script replaces the multiintersection of all roadmap tissues after each being crossed to villar with a pandas query.

#180205 METHOD 2 - this script has been modified from the 180130 script comparing roadmap hliver active enhancers with villar liver enhancers into a query for  any roadmap active enhancer dataset with evidence of Villar liver enhancers. The purpose of this query is to understand the uniqueness or the ubiquity of Villar hliver enhancers in other roadmap tissues. Identifying active enhancers in Villar's dataset present in the roadmap datasets in any tissue may validate those fragments as bona fide enhancers.

#180206 - The relaxed enhancer definition should  be used to recapitulate Villar's method for identifying enhancers in the future. For now, I will use the stringent method for orthogonal validation of enhancers with the multiintersect approach used to identify broadly active enhancers. 

#180130 the purpose of this script is to ask how many enhancers identified by Villar histone modifications in human liver overlap enhancers identified in roadmap human livers identified by those same histone modifications.

# Villar Enhancer = H3K27ac+ H3K4me3- with no/ <50% overlap with H3K4me3 ChIP-SEQ dataset.

#'s' = 'stringent' enhancers where H3K27ac modifications absolutely do not overlap H3K4me3 modification fragments.(H3K27ac+/H3K4me3-)
#'r' = 'relaxed' enhancers here H3K27ac modifications do not overlap H3K4me3 modification fragments, or do with less than 50% overlap (H3K27ac+/H3K4me3<50%)

import os, sys, random
import pandas
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np

now = datetime.date.today()
now_dir = "/Users/sarahfong/Desktop/CAPRA/%s" %now
mkdir = "mkdir %s" %now_dir
os.system(mkdir)

path = "/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-22"

os.chdir(path)

roadmap_datasets= sorted(glob.glob("2018-01-2*.bed"))

date = path.split("/")[-1]

roadmap_datasets_dict= dict((i.split("_")[6], i) for i in roadmap_datasets) # this is a dictionary of E00*.bed:2018-01-2*.bed

test = "HSap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed" # the villar hliver active enhancer dataset.
roadmap_datasets_dict["villar"] = test
test = list(roadmap_datasets_dict.values())
#test_list = random.sample(test, 5)

test_list = ['2018-01-22_Hsap_H3K27ac_plus_H3K4me3_minus_E061.bed','2018-01-22_Hsap_H3K27ac_plus_H3K4me3_minus_E119.bed','2018-01-22_Hsap_H3K27ac_plus_H3K4me3_minus_E080.bed','2018-01-22_Hsap_H3K27ac_plus_H3K4me3_minus_E008.bed','2018-01-22_Hsap_H3K27ac_plus_H3K4me3_minus_E114.bed', '2018-01-22_Hsap_H3K27ac_plus_H3K4me3_minus_E059.bed']
test_list.append('2018-01-22_Hsap_H3K27ac_plus_H3K4me3_minus_E066.bed')
test_list.append('HSap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed')

for test in test_list:
    
    if 'HSap' not in test:
        test_tissue = (test.split("_")[6]).replace(".bed", "")
    else:
        test_tissue = "villar"
    v = pandas.read_table(test)

    v.columns=["test-chr", "test-start", "test-end", "counts"]
    v["ID"] = v[["test-chr", "test-start", "test-end"]].astype(str).sum(axis=1)

    #perform single intersect to find overlapping test hliver enhancers in E-xxx_tissue

    df = pandas.DataFrame(columns= ["tissue","num_roadmap", "num_test","num_test_u", "overlap", "overlap_u"]) #empty dataframe to measure the overlap of roadmap_datasets and villar_dataset for each tissue.

    for tissue, tissue_file in roadmap_datasets_dict.items():
        tissue = tissue.replace(".bed", "")
    
        outfile = "%s_test_%s_enhancers_x_%s_%s.bed" % (now,test_tissue, date, tissue)
        outfile_u = "%s_test_%s_enhancers_u_x_%s_%s.bed" % (now, test_tissue, date, tissue)

        bed_cmd = "bedtools intersect -a %s -b %s -f 0.5 -e -wa -wb > %s" % (test, tissue_file, outfile)
        #print(bed_cmd)
        os.system(bed_cmd)
    
        bed_cmd_u = "bedtools intersect -a %s -b %s -f 0.5 -e -u > %s" % (test, tissue_file, outfile_u)
        #print(bed_cmd_u)
        os.system(bed_cmd_u)
    
        tissue_enh_count = len(pandas.read_table(tissue_file))
        villar_enh_count = len(pandas.read_table(outfile))
        villar_u_enh_count = len(pandas.read_table(outfile_u))
        overlap = (villar_enh_count/tissue_enh_count)
        overlap_u = (villar_u_enh_count/tissue_enh_count)

        df2 = pandas.DataFrame([[tissue, tissue_enh_count, villar_enh_count, villar_u_enh_count, overlap, overlap_u]], columns= ["tissue","num_roadmap", "num_test","num_test_u", "overlap", "overlap_u"])
        df = pandas.concat([df2, df])

    width = 0.7 
    indices = np.arange(len(df["tissue"]))

    df= df.sort_values(by ="num_test_u", ascending=False)

    ################## Bar Plot VILLAR ENHANCER OVERLAP PER ROADMAP TISSUE  ##############################

    plt.figure(num=None, figsize=(10,6), dpi=200, facecolor='w', edgecolor='k')

    plt.bar(indices, df["num_roadmap"], width, color = 'k', label = 'Roadmap', alpha = 0.25)
    
    #plt.bar([(i+0.25*width) for i in indices], df["num_test"], 0.5*width, color = 'r', alpha = 0.5, label = 'test-Overlap All')

    plt.bar([(i-0.25*width) for i in indices], df["num_test_u"], 0.5*width, color = 'r', alpha = 0.5, label = 'Test-Overlap') #Unique Villar Enhancers

    plt.xticks(indices,[i for i in df["tissue"]], fontsize = 6, rotation=90)
    plt.yticks(fontsize = 10)
    plt.ylabel("Num. Enhancer Peaks", fontsize = 10)

    plt.xlabel("Roadmap Datasets", fontsize = 10)
    plt.title("Abs Num. Overlapping %s Peaks in Roadmap" % test_tissue, fontsize = 10)
    plt.legend(fontsize = 10)
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)

    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_test_%s_overlap_abs.pdf" %(now, now, test_tissue))
    plt.close()
    
    ###################### Line plot VILLAR ENHANCER OVERLAP PER ROADMAP TISSUE ###################

    plt.figure(num=None, figsize=(8,6), dpi=100, facecolor='w', edgecolor='k')
    
    plt.plot(indices, df["num_roadmap"], width, color = 'k', label = 'Roadmap', alpha = 0.25)
    plt.plot(indices, df["num_test"], width, color = 'b', label = 'Test-All ')
    plt.plot(indices, df["num_test_u"], width, color = 'r', label = 'Test-Unique')
    
    plt.xticks(indices,[i for i in df["tissue"]], fontsize = 6, rotation = 90)
    plt.ylabel("Num. Enhancer peaks")
    plt.xlabel("Roadmap Datasets")
    
    plt.title("Abs Num. Overlapping %s  Peaks in Roadmap" % test_tissue)
    plt.legend()

    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_test_%s_overlap_line.pdf" % (now,now, test_tissue))
    #plt.show()
    plt.close()
    
    #################### Horizontal Bar Plot FREQUENCY OF VILLAR OVERLAP PER ROADMAP TISSUE ################# #####
    
    df= df.sort_values(by ="overlap_u", ascending = False)
    plt.figure(num=None, figsize = (10,4), dpi=200, facecolor= 'w', edgecolor ='k')
    
    plt.bar(indices, df["overlap_u"], width, color = 'g', label = 'Roadmap Enhancers')
    #plt.barh(indices, df["overlap_u"], width, color = 'g', label = 'Villar Enhancers')
    
    plt.xticks(indices+0.5*width,[i for i in df["tissue"]], fontsize = 6, rotation = 90)
    #plt.yticks(indices,[i for i in df["tissue"]], fontsize = 5)
    
    plt.ylabel("Overlap", fontsize=8)
    
    plt.xlabel("Roadmap Datasets", fontsize=8)
    plt.legend(fontsize=8)
    plt.title("Freq Overlapping Unique %s Enhancers in Roadmap Tissues" % test_tissue, fontsize=8)
    
    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_test_%s_overlap_freq.pdf" % (now,now, test_tissue))
    plt.close()
    
    ############## INTERSECT ALL UNIQUE VILLAR ACTIVE ENHANCERS PER ROADMAP TISSUE TO FIND COMMONLY OVERLAPPING ENHANCERS BETWEEN DATASETS  #########
    x= "%s_test_%s_enhancers_u*.bed" % (now, test_tissue)
    any_tissue_enhancer=glob.glob(x)
    rm_test= "%s_test_%s_enhancers_u_x_%s_%s.bed" % (now, test_tissue, date, test_tissue)
    any_tissue_enhancer.remove(rm_test)
    results = v
    
    for tissue in any_tissue_enhancer:

        df= pandas.read_table(tissue)
    
        tissue_id = (tissue.split("_")[7]).split(".")[0]

        df.columns = ["test-chr", "test-start", "test-end", "counts"]
        df["ID"] = df[["test-chr", "test-start", "test-end"]].astype(str).sum(axis=1)
        df["%s"%tissue_id] = 1

        results = pandas.merge(results, df[["ID","%s" %tissue_id]], how = "left", on = "ID")

    a = results.columns[4:]
    results["sum"] = results[a].sum(axis =1)
    results["sum"]=results["sum"].fillna(0)
    
    b = results["sum"].quantile(0.9) # the zeros do not change the 90th % cut off of overlapping datasets. 
    results_90 = results.loc[results["sum"]>= b]
    results_90["90_percentile"] = 1
    results_merged = pandas.merge(results, results_90, how = "left")
    results_merged["90_percentile"] = results_merged["90_percentile"].fillna(0)

    #results_90.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/method2_villar_x_any_roadmap_90th_%s.bed" % (now,now), sep = '\t', index = False, header = False)
    
    #results_merged.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/method2_villar_x_any_roadmap_%s.csv" % (now,now), sep = '\t', index = False, header = True)

    ##################### PLOT 90TH% AS A HISTOGRAM ############
    hist_data = results["sum"]
    hist90_data = results_90["sum"]
    indices = np.arange(results["sum"].max())
    data,bins,patches = plt.hist(hist_data, indices, normed = True, facecolor = "blue", alpha = 0.75, label = "All Enhancers")
    data,bins,patches = plt.hist(hist90_data, indices, normed = True, facecolor = "yellow", alpha = 0.75, label = "90th% Enhancers")

    plt.xlabel("Num. Overlapping Roadmap Datasets")
    plt.ylabel("Num. %s Enhancers" % test_tissue)
    plt.title("Freq Overlapping %s Enhancers in Roadmap Datasets"%test_tissue)
    plt.legend()
    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_test_%s_enhancers_normed_overlapping_roadmap.pdf" %(now,now, test_tissue))
    plt.close()

"""
############COMPARE TO VILLAR ACTIVE ENHANCERS IN OTHER SPECIES#########3

path = "/Users/sarahfong/Desktop/CAPRA/datasource/"
os.chdir(path)
villar_species= pandas.read_table("Hsap_H3K27ac_humanspEnhancers.bed", sep =',')

#formatting
villar_species= villar_species.fillna(0) # 'NA' = enhancer sequences is not conserved (alignable) in species
villar_species = villar_species.replace("-", 1) # "-" = enhancer sequence is conserved (alignable) in species, but not active in species' liver. 

species = villar_species.columns[4:] # all species in Villar
villar_species["num_species"]= villar_species[species].sum(axis=1)
high_quality_species =[' Mmul', 'Cjac', 'Mmus', 'Rnor', ' Ocun', 'Btau', ' Sscr', 'Cfam', 'Fcat']

villar_species["num_high_quality_species"] = villar_species[high_quality_species].sum(axis = 1)

vrs = pandas.merge(results, villar_species, how = "left", on= "IDs")
vrs.sort_values(by = "sum")

#vrs.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/Villar_Roadmap_Species_overlap_%s" %(now, now), sep = '\t', index = False, header = False)

short_vrs = vrs[["v-chr", "v-start", "v-end","IDs", "sum", "num_species", "num_high_quality_species"]]
short_vrs= short_vrs.sort_values(by = "sum")
short_vrs["num_species"] = short_vrs["num_species"].fillna(0)
short_vrs["num_high_quality_species"] = short_vrs["num_high_quality_species"].fillna(0)
short_vrs_90 = short_vrs.loc[short_vrs["sum"]>= b]


############# SPECIES V. TISSUES DATA TO PLOT ##############

sum_roadmap = short_vrs["sum"].loc[short_vrs["sum"]>0] # Villar enhancers overlapping roadmap
num_species = short_vrs["num_species"].loc[short_vrs["sum"]>0].fillna(0) #0=Enhancers not in Villar v. species dataset. Not sure if this a true negative (queried for enhancer alignability and found none in other species) or a false negative (never queried for enhancer alignability in other species).
num_hq_species = short_vrs["num_high_quality_species"].loc[short_vrs["sum"]>0].fillna(0)

sum_villar =  short_vrs["sum"].loc[short_vrs["sum"]==0] # Villar enhancers overlapping no roadmap tissue datasets
num_species_villar =  short_vrs["num_species"].loc[short_vrs["sum"]==0] # Villar enhancers overlapping no roadmap tissue datasets
num_hq_species_villar = short_vrs["num_high_quality_species"].loc[short_vrs["sum"]==0] # Villar enhancers overlapping no roadmap tissue datasets

sum_roadmap_90 = short_vrs_90["sum"]
num_species_90 = short_vrs_90["num_species"]
num_hq_species_90 = short_vrs_90["num_high_quality_species"]
########### Plot the distribution of # species sequence v. # of tissues #########

plt.figure(num=None, figsize = (8,6), dpi=200, facecolor= 'w', edgecolor ='k')
plt.scatter( num_species,sum_roadmap, c= "blue", alpha= 0.5, label = "Villar Active Enh Overlapping All Roadmap")
plt.scatter( num_species_90,sum_roadmap_90, c = "yellow", alpha = 0.5,label = "Villar Active Enh Overlapping 90% Roadmap")
plt.scatter( num_species_villar, sum_villar, c= "red", alpha = 0.5, label =  "Villar Active Enh Not Overlapping Roadmap")

plt.scatter( num_hq_species,sum_roadmap, c= "green", alpha= 0.75, label = "HQ-Villar Active Enh Overlapping All Roadmap")
plt.scatter (num_hq_species_90, sum_roadmap_90,c = "orange", alpha = 0.75,label = "HQ-Villar Active Enh Overlapping 90% Roadmap")
plt.scatter( num_hq_species_villar,sum_villar, c= "purple", alpha= 0.75, label = "HQ-Villar Active Enh Overlapping All Roadmap")
plt.ylabel("Num. Overlapping Roadmap Datasets")

#plt.ylim(ymax = 20)
plt.xticks(np.arange(0, 19, 2))
plt.xlabel("Num. Species -  Enh Sequence Alignments")
plt.legend()
plt.title(" Num. Species Enh Sequence Alignments \n x Num. Overlapping Roadmap Active Enhancers")

plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_overlap_roadmap_x_species_%s.pdf" % (now,now,now))
plt.close()

############### Histogram of Villar enhancers distributed over the number of species #########

indices = np.arange(short_vrs["num_species"].max())# when plotting all 18 species
indices = np.arange(short_vrs["num_high_quality_species"].max()) # when plotting only 9 HQ datasets.
#data,bins,patches = plt.hist(num_species, indices, normed= True, facecolor = "blue", alpha = 0.75, label = "All Villar Enhancers")
data,bins,patches = plt.hist(num_hq_species, indices, normed= True, facecolor = "purple", alpha = 0.5, label = "HQ-All Villar Enhancers")
#data,bins,patches = plt.hist(num_species_90, indices, normed= True, facecolor = "yellow", alpha = 0.5, label = "90th% Villar x Roadmap Enhancers")

data,bins,patches = plt.hist(num_hq_species_90, indices, normed= True, facecolor = "red", alpha = 0.15, label = "HQ-90th% Villar x Roadmap Enhancers")

plt.xticks(np.arange(0, 12, 2))
plt.xlabel("Num. Species Shared Sequence Alignments")
#plt.ylabel("Num. Active Enhancers") # Use when normed = False.
plt.ylabel("Frequency of Active Enhancers")
plt.title("Frequency Of Broadly Active Enhancers Across Species")
plt.legend()
plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_species_hist_90_normalized_hq.pdf" %(now,now))
plt.close()

"""
