# this is a script to get the 90% percentile of enhancer fragments from stringent and relaxed enhancer criteria.
# the second purpose of this script is to plot the number of stringet, relaxed, merged, unmerged enhancers that fall in that 90st percentile for visualization.

#!/bin/bash/python

#180129
#sarahfong

import pandas
import numpy
import matplotlib.pyplot as plt
import os

#'s, strg' = 'stringent' enhancer definition, where there is NO overlap between H3K27ac and H3K4me3 ChIP-Seq peaks. Files associated with this analysis were done 2018-01-22

#'r, relx' = 'relaxed' enhancer definition, where there are non-overlapping H3K27ac and H3K4me3 ChIP-Seq peaks and H3K4me3 peaks that overlap <50% with H3K27ac. Files associated with this analysis were done 2018-01-23

#'merged' = enhancer fragments that were merged
os.chdir("../2018-01-29")
s_infile = "2018-01-22_small_all_tissue.csv"
r_infile = "2018-01-23_small_all_tissue_Hsap.csv"

df_s = pandas.read_table(s_infile)
len_df_s = len(df_s["chrom"])

df_r = pandas.read_table(r_infile)
len_df_r = len(df_r["chrom"])

#####BAR PLOT OF ENHANCERS +/- OVERLAP################3
"""
fig, ax = plt.subplots()

ind=1
width = 0.3

plt_s= ax.bar(ind, len_df_s, width,  alpha = 0.5, label ="stringently non-overlapping")

plt_r = ax.bar(ind+width, len_df_r, width, alpha=0.5, label = "relaxed non-overlapping + overlapping") 


plt.legend(loc = "upper left")
plt.xlabel('')
plt.ylabel("No.Enhancer Fragments")
plt.title("Total Enhancers \n All Tissues - Roadmap")
plt.show()
"""
#########GET 90th PERCENTILE#########

df_s90 = df_s.loc[df_s["num"]>=df_s["num"].quantile(0.90)] 

#df_s90 = df_s90[[ "chrom", "start", "end", "num"]]

liver_count = df_s90["list"].str.contains("E108.bed").sum()

df_r90 = df_r.loc[df_r["num"]>=df_r["num"].quantile(0.90)]

#df_r90 = df_r90[[ "chrom", "start", "end", "num"]]
liver_count_r = df_r90["list"].str.contains("E108.bed").sum()
s90_file = "df_s90.bed"
#df_s90.to_csv(s90_file, sep = '\t', header = False, index = False)

r90_file = "df_r90.bed"
#df_r90.to_csv(r90_file, sep = '\t', header = False, index = False)

########## MERGE THE 90TH PERCENTILE #################

merge_s = "df_s90_merged_no_name.bed"

merge_r= "df_r90_merged_no_name.bed"

cmd_s= "mergeBed -i %s -d 100 -c 4 -o collapse -delim ',' > '%s'"% (s90_file, merge_s)

cmd_r= "mergeBed -i %s -d 100 -c 4 -o collapse -delim ',' > '%s'"% (r90_file, merge_r)


os.system(cmd_s) # 99th = 4785 enhancers, 90th - 38371
os.system(cmd_r) # 99th = 5665 enhancers, 90th = 29969 

#### JACCARD TO MEASURE OVERLAP BETWEEN UNMERGED/ MERGED S90, R90 ######

cmd_unmerged = "bedtools jaccard -a %s -b %s -f 0.5 -r" %(s90_file,r90_file)  # q: what is the overlap of unmerged 90th stringent/ relaxed peaks?

cmd_merged = "bedtools jaccard -a %s -b %s -f 0.5 -r" %(merge_r, merge_s) # q: what is the overlap of merged 90th stringent/ relaxed peaks

os.system(cmd_unmerged)
os.system(cmd_merged)

#### ROADMAP 90th percentile v. VILLAR ####

villar = "HSap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed" # Villar dataset

villar_s_file = "villar_df_s90.bed"
cmd_villar_s = "bedtools intersect -a %s -b %s -f 0.5 -r > '%s'" % (villar, s90_file, villar_s_file)

villar_r_file = "villar_df_r90.bed"
cmd_r_villar = "bedtools intersect -a %s -b %s -f 0.5 -r > '%s'" % (villar,r90_file, villar_r_file)

vr_df = pandas.read_table(villar_r_file)
vs_df = pandas.read_table(villar_s_file)

villar_jaccard= 'bedtools jaccard -a villar_df_r90.bed -b villar_df_s90.bed -f 0.5 -r'

