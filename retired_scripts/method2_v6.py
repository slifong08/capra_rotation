 #!/bin/bash/python

#180228 step 1 - make 10000 random length matched shuffle bed files.
# step 2 - load each file into a for loop
 #180216 "High Quality" (HQ) villar species ChIP-SEQ datasets were separately analyzed versus Broadly Active Enhancers (BAEs) overlapping the 90th% of enhancers identified in Method 2. We have found that most BAEs do not contain any sequence that aligns with other species, suggesting these enhancers correspond to human specific sequences in regions of the genome recently acquired. To make sure that our finding is not an artifact of noisy sequencing datasets,  we limited our comparison to the high_quality_species datasets.

#180210 METHOD 2 V1 - This script replaces the multiintersection of all roadmap tissues after each being crossed to villar with a pandas query.

#180205 METHOD 2 - this script has been modified from the 180130 script comparing roadmap hliver active enhancers with villar liver enhancers into a query for  any roadmap active enhancer dataset with evidence of Villar liver enhancers. The purpose of this query is to understand the uniqueness or the ubiquity of Villar hliver enhancers in other roadmap tissues. Identifying active enhancers in Villar's dataset present in the roadmap datasets in any tissue may validate those fragments as bona fide enhancers.

#180206 - The relaxed enhancer definition should  be used to recapitulate Villar's method for identifying enhancers in the future. For now, I will use the stringent method for orthogonal validation of enhancers with the multiintersect approach used to identify broadly active enhancers. 

#180130 the purpose of this script is to ask how many enhancers identified by Villar histone modifications in human liver overlap enhancers identified in roadmap human livers identified by those same histone modifications.

# Villar Enhancer = H3K27ac+ H3K4me3- with no/ <50% overlap with H3K4me3 ChIP-SEQ dataset.

#'s' = 'stringent' enhancers where H3K27ac modifications absolutely do not overlap H3K4me3 modification fragments.(H3K27ac+/H3K4me3-)
#'r' = 'relaxed' enhancers here H3K27ac modifications do not overlap H3K4me3 modification fragments, or do with less than 50% overlap (H3K27ac+/H3K4me3<50%)

import os, sys
import pandas
import glob
import datetime
print("start ",datetime.datetime.now())

## collection dataframe

random_results = pandas.DataFrame()
columns =["random_file", "min", "25th", "50th", "75th", "max", "median", "mean"]
today = datetime.date.today()
now = today
test_path = "/home/fongsl/test" # ACCRE TEMP

test_path = '/Users/sarahfong/accre_repo' # LOCAL TEMP
mkdir = "mkdir %s "% test_path

os.system(mkdir)

blacklist = "/dors/capra_lab/bentonml/data/hg19_blacklist_gap.bed"
trim_chrom = "/dors/capra_lab/data/dna/hg19_trim.chrom.sizes"
v_90th = "/home/fongsl/accre_repo/results_villar_90_v1.bed"
shuffle_val = 1

while shuffle_val < 10:
   random = "random-%s.bed" % shuffle_val
   r = "random-%s" %shuffle_val
   shuffle_cmd = "shuffleBed -excl %s -i %s -g %s -chrom -noOverlapping > %s/%s" % (blacklist, v_90th, trim_chrom, test_path, random)
   #print(shuffle_cmd)
   os.system(shuffle_cmd)
   
   shuffle_val = shuffle_val +1
   
   os.chdir(test_path)
   random = "random_9.bed"
   v = pandas.read_table(random)
   v_file = test_path+"/"+random
   accre_path = "/home/fongsl/roadmap/2018-01-22_stringent" # ACCRE
   accre_path = "/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-22" # LOCAL
   os.chdir(accre_path)

   roadmap_datasets= sorted(glob.glob("2018-01-2*.bed"))

   date = accre_path.split("/")[-1]

   roadmap_datasets_dict= dict((i.split("_")[-1], i) for i in roadmap_datasets) # this is a dictionary of E00*.bed:2018-01-2*.bed

   ####begin step2 analysis

   v.columns=["v-chr", "v-start", "v-end"]

   #perform single intersect to find overlapping villar hliver enhancers in E-xxx_tissue

   for tissue, tissue_file in roadmap_datasets_dict.items():
      tissue = tissue.replace(".bed", "")
    
      outfile_u = "%s/%s_%s_u_x_%s.bed" % (test_path, now, r, tissue)
    
      bed_cmd_u = "bedtools intersect -a %s -b %s -f 0.5 -e -u > %s" % (v_file, tissue_file, outfile_u)
      #print(bed_cmd_u)
      os.system(bed_cmd_u)  

   ############## INTERSECT ALL UNIQUE VILLAR ACTIVE ENHANCERS PER ROADMAP TISSUE TO FIND COMMONLY OVERLAPPING ENHANCERS BETWEEN DATASETS  #########
   os.chdir(test_path)
   
   x = ("%s_%s_u_x*.bed") % (now, r)
   any_tissue_enhancer=glob.glob(x)

   results = v

   for tissue in any_tissue_enhancer:
      
      df= pandas.read_table(tissue)
    
      tissue_id = (tissue.split("_")[-1]).split(".")[0]

      df.columns =["v-chr", "v-start", "v-end"]
      df["%s"%tissue_id] = 1

      results = pandas.merge(results, df, how = "left")

   a = results.columns[3:]
   results["sum"] = results[a].sum(axis =1)
   ran_min = results["sum"].min()
   ran_max = results["sum"].max()
   rq25 = results["sum"].quantile(0.25)
   rq50 = results["sum"].quantile(0.50)
   rq75 = results["sum"].quantile(0.75)
   rmed = results["sum"].median()
   rmean = results["sum"].mean()
   insert = [r, ran_min, rq25, rq50, rq75, ran_max, rmed, rmean]
   insert = pandas.Series(insert)
   random_results=random_results.append(insert, ignore_index = True)

   erase = "rm %s_%s_u_x*.bed" % (now, r)
   os.system(erase)

random_results.to_csv("%s_random_results.csv" % now, sep = '\t', index = False, header = True)

print("end",datetime.datetime.now())
