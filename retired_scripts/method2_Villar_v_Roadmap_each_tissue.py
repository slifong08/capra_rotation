#!/bin/bash/python

#180205 METHOD 2 - this script has been modified from the 180130 script comparing roadmap hliver active enhancers with villar liver enhancers into a query for  any roadmap active enhancer dataset with evidence of Villar liver enhancers. The purpose of this query is to understand the uniqueness or the ubiquity of Villar hliver enhancers in other roadmap tissues. Identifying active enhancers in Villar's dataset present in the roadmap datasets in any tissue may validate those fragments as bona fide enhancers.

#180206 - The relaxed enhancer definition should  be used to recapitulate Villar's method for identifying enhancers in the future. For now, I will use the stringent method for orthogonal validation of enhancers with the multiintersect approach used to identify broadly active enhancers. 

#180130 the purpose of this script is to ask how many enhancers identified by Villar histone modifications in human liver overlap enhancers identified in roadmap human livers identified by those same histone modifications.

# Villar Enhancer = H3K27ac+ H3K4me3- with no/ <50% overlap with H3K4me3 ChIP-SEQ dataset.

#'s' = 'stringent' enhancers where H3K27ac modifications absolutely do not overlap H3K4me3 modification fragments.(H3K27ac+/H3K4me3-)
#'r' = 'relaxed' enhancers here H3K27ac modifications do not overlap H3K4me3 modification fragments, or do with less than 50% overlap (H3K27ac+/H3K4me3<50%)

import os, sys
import pandas
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np

now = datetime.date.today()

now = "2018-02-05"
#enh_dir = ["/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-22", "/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-23"]

path = "/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-22"
#enh_dir = ["/home/fongsl/roadmap/2018-01-22_stringent/", "/home/fongsl/roadmap/2018-01-23_relaxed/"] # ACCRE directories contains roadmap active enhancer datasets defined by two different definitions.
#for path in enh_dir:

os.chdir(path)
roadmap_datasets= sorted(glob.glob("2018-01-2*.bed"))
date = path.split("/")[-1]

roadmap_datasets_dict= dict((i.split("_")[6], i) for i in roadmap_datasets) # this is a dictionary of E00*.bed:2018-01-2*.bed

villar = "HSap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed" # the villar hliver active enhancer dataset.
v = pandas.read_table(villar)
v.columns=["chr", "start", "end", "villar"]
#perform single intersect to find overlapping villar hliver enhancers in E-xxx_tissue

df = pandas.DataFrame(columns= ["tissue","num_roadmap", "num_villar", "overlap"])

for tissue, tissue_file in roadmap_datasets_dict.items():
    tissue = tissue.replace(".bed", "")
    
    outfile = "%s_villar_enhancers_x_%s_%s.bed" % (now, tissue, date)
    
    #print("the outfile: ", outfile)
    bed_cmd = "bedtools intersect -a %s -b %s -f 0.5 -e -sorted -wa -wb > /Users/sarahfong/Desktop/CAPRA/2018-02-05/%s" % (villar, tissue_file, outfile)
    #print ("the bed_cmd", bed_cmd)
    #os.system(bed_cmd)
    
    tissue_enh_count = len(open(tissue_file, "r").readlines()) # counts the number of rows in the roadmap file.
    villar_enh_count = len(open(outfile, "r").readlines()) # counts the number of rows in the overlapping villar roadmap file.
    overlap = (villar_enh_count/tissue_enh_count)
    print(overlap)
    df2 = pandas.DataFrame([[tissue, tissue_enh_count, villar_enh_count, overlap]], columns= ["tissue","num_roadmap", "num_villar", "overlap"])
    df = pandas.concat([df2, df])

width = 0.7 
indices = np.arange(len(df["tissue"]))

df= df.sort_values(by ="overlap")

################## Horizontal Bar Plot VILLAR ENHANCER OVERLAP PER ROADMAP TISSUE  ##############################

plt.figure(num=None, figsize=(4,6), dpi=200, facecolor='w', edgecolor='k')

plt.barh(indices, df["num_roadmap"], width, color = 'b', label = 'Roadmap Enhancers')

plt.barh([(i+0.25*width) for i in indices], df["num_villar"], 0.5*width, color = 'r', alpha = 0.5, label = 'Villar-overlap')

plt.yticks(indices,[i for i in df["tissue"]], fontsize = 3.75)
#plt.xticks(indices,[i for i in df["tissue"], fontsize = 4, rotation = 90)
plt.xlabel("num peaks", fontsize = 6)

plt.ylabel("roadmap datasets", fontsize = 6)
plt.title("Abs num. Villar Enhancers overlapping in Roadmap", fontsize = 6)

plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)

plt.savefig("/Users/sarahfong/Desktop/CAPRA/2018-02-05/%s_villar_overlap_bar_abs.pdf" % date)
plt.close()
    
###################### Line plot VILLAR ENHANCER OVERLAP PER ROADMAP TISSUE ###################

plt.figure(num=None, figsize=(8,6), dpi=100, facecolor='w', edgecolor='k')

plt.plot(indices, df["num_roadmap"], width, color = 'b', label = 'Roadmap Enhancers')
plt.plot(indices, df["num_villar"], width, color = 'r', label = 'Villar Enhancers')

#plt.plot(([(i+0.25*width) for i in indices]), df["num_villar"], 0.5*width, color = 'r', alpha = 0.5, label = 'Villar-overlap')

plt.xticks(indices,[i for i in df["tissue"]], fontsize = 6, rotation = 90)
plt.ylabel("num peaks")
plt.xlabel("roadmap datasets")

plt.title("villar overlap with roadmap enhancers %s" % date)
plt.legend()
#plt.text([i for i in indices], [i for i in df["num_villar"], [i for i in villar_enh_list])
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.savefig("/Users/sarahfong/Desktop/CAPRA/2018-02-05/%s_method2_villar_overlap_line.pdf" % date)
plt.show()
plt.close()

#################### Horizontal Bar Plot FREQUENCY OF VILLAR OVERLAP PER ROADMAP TISSUE ################# #####

plt.figure(num=None, figsize = (6,8), dpi=200, facecolor= 'w', edgecolor ='k')

plt.barh(indices, df["overlap"], width, color = 'g', label = 'Roadmap Enhancers')

"""rect_labels=[]
for rect in rects:
    w = int(rect.get_width())
    xloc = 0.98 * w
    yloc = rect.get_y() + rect.get_height()/2.0
    label = plt.text(xloc, yloc, rect)
    rect_labels.append(label)
"""    

plt.yticks(indices,[i for i in df["tissue"]], fontsize = 5)
#plt.xticks(indices,[i for i in tissue_id_list], fontsize = 4, rotation = 90)
plt.xlabel("Frequency of Villar overlap in Roadmap Tissue dataset")
#plt.xlim(1)
plt.ylabel("roadmap datasets")

plt.title("Villar Overlap in Roadmap Tissues")

plt.savefig("/Users/sarahfong/Desktop/CAPRA/2018-02-05/%s_method2_freq_villar_overlap_in_roadmap_%s.pdf" % (now, date))
    
############## INTERSECT ALL VILLAR ACTIVE ENHANCERS PER ROADMAP TISSUE TO FIND COMMONLY OVERLAPPING ENHANCERS BETWEEN DATASETS  #########

any_tissue_enhancer=glob.glob("%s_villar_enhancers_x_E*.bed" % now)
a = ' '.join(str(i) for i in any_tissue_enhancer)
b = ' '.join(str(i).split("_")[4] for i in any_tissue_enhancer)# a string of the tissue ids
   
output_multi = "%s_method2_villar_x_any_roadmap_multi.bed" % now
output_int_u = "%s_method2_villar_x_any_roadmap_int-u.bed" % now
output_int_c = "%s_method2_villar_x_any_roadmap_int-c.bed" % now

os_multi_cmd = "multiIntersectBed -i %s -sorted -header -names %s > /Users/sarahfong/Desktop/CAPRA/2018-02-05/%s" % (a,b, output_multi) # the multiintersect gives count of overlapping records
os_int_cmd_u = "Bedtools intersect -a %s -b %s -sorted -header -f 0.5 -e -wa -u  > /Users/sarahfong/Desktop/CAPRA/2018-02-05/%s" % (villar, a, output_int_u) # unique intersect tells us how many villar records are found in any tissue.
os.system(os_multi_cmd)
os.system(os_int_cmd_u)

os_int_cmd_c = "Bedtools intersect -a %s -b %s -sorted -header -f 0.5 -e  > /Users/sarahfong/Desktop/CAPRA/2018-02-05/%s" % (villar, a, output_int_c) # unique intersect tells us how many villar records are found in any tissue.

os.system(os_int_cmd_c)

###################### GET 90TH% AND MERGE ###

os.chdir("/Users/sarahfong/Desktop/CAPRA/2018-02-05")
group_by_file = "group_by_file.bed"
group_by = "bedtools groupby -i %s -g 4 -c 4 -o count > %s" % (output_int_c, group_by_file)
os.system(group_by)
df = pandas.read_table(group_by_file)
df.columns = ["villar", "num"]
df=pandas.merge(v,df, on= "villar")

df["len"] = df["end"].subtract(df["start"])
df_90 = df.loc[df["num"]>=df["num"].quantile(0.9)]

df_90.to_csv("method2_villar_x_any_roadmap_90th.bed", sep = '\t', index = False, header = False)

##################### PLOT 90TH% AS A HISTOGRAM ############
hist_data = df["num"]
hist90_data = df_90["num"]
indices = np.arange(len(df["num"].unique()))
data,bins,patches = plt.hist(hist_data, indices, facecolor = "blue", alpha = 0.75, label = "all tissues")
data,bins,patches = plt.hist(hist90_data, indices, facecolor = "yellow", alpha = 0.75, label = "90th% tissues")

    
plt.xlabel("number of overlapping roadmap datasets")
plt.ylabel("count of peak fragments")
plt.title("frequency of villar enhancer peaks in roadmap tissues-%s" % date)
plt.legend()
plt.savefig("/Users/sarahfong/Desktop/CAPRA/2018-02-05/%s_method2_villar_enhancers_overlapping_roadmap.pdf" %date)
plt.close()
