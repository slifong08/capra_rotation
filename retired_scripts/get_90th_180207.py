#180207 this script is modified from 180202, which was modified from the original script written 180129.

#180207 in this analysis, I will merge only the stringently overlapping enhancer fragments identified from a multiintersect of 98 roadmap active enhancer datasets performed on 180122. Those enhancers were multiintersected using a stringent definition of an active enhancer, where H3K27ac histone modifications with no overlapping H3K4me3 modifications overlapped those peaks. Multiintersected Bed tools command does not take into account the fraction of overlap. It only reports how much each fragment overlaps with all the other fragments in the pool. In this case, a fragement that does not overlap with other fragments will be written into the results of a multiintersect Bed file as only intersecting with itself.

#For identifying active enhancers overlapping in the Villar and Roadmap datasets, I will require that merged enhancer fragments be greater than 50bp in length. 50bp is a subjective cut off and defined by a comfortable minimum length for an enhancer. 


#This is a script to get the 90% percentile of enhancer fragments from stringent and relaxed enhancer criteria.
# the second purpose of this script is to plot the number of stringent, relaxed, merged, unmerged enhancers that fall in that 90st percentile for visualization.

#'s, strg' = 'stringent' enhancer definition, where there is NO overlap between H3K27ac and H3K4me3 ChIP-Seq peaks. Files associated with this analysis were done 2018-01-22

#'r, relx' = 'relaxed' enhancer definition, where there are non-overlapping H3K27ac and H3K4me3 ChIP-Seq peaks and H3K4me3 peaks that overlap <50% with H3K27ac. Files associated with this analysis were done 2018-01-23

#'merged' = enhancer fragments that were merged

#!/bin/bash/python

#180129
#sarahfong

import pandas
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt

now =  dt.date.today()

data_path = "/Users/sarahfong/Desktop/CAPRA/datasource/"
os.chdir(data_path)

villar = "/Users/sarahfong/Desktop/CAPRA/datasource/HSap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed" 

s_infile = "2018-01-22_small_all_tissue_Hsap.csv"

df_s = pandas.read_table(s_infile)
df_s = df_s[["chrom", "start", "end", "num"]] # get rid of the index column transferred from the last pandas query.

working_directory = "/Users/sarahfong/Desktop/CAPRA/%s" % now
os.chdir(working_directory)

df_s["len"] = df_s["end"].subtract(df_s["start"])

df_s50 =  df_s.loc[df_s["len"] >= 50]

#####HISTOGRAM OF FRAGMENT LENGTHS IN ROADMAP MULTIINTERSECT DATAFRAME ######

frag_len = df_s["len"]
frag_len_50 = df_s["len"].loc[df_s["len"] >= 50]

x = len(frag_len_50)/len(frag_len) # % of active enhancer fragments >=50bp after multiintersecting 98 active enhancer datasets

ind=np.arange(len(df_s["len"].unique()))
width = 0.3

plt_s, bins, patch= plt.hist(frag_len, ind,  alpha = 0.5, label ="frag length all lengths")
plt_s, bins, patch= plt.hist(frag_len_50, ind, alpha = 0.5,label ="frag length >= 50bp")

plt.legend(loc = "upper right")
plt.xlabel('Multiintersect stringent enhancer frag length')
plt.ylabel("count")

plt.title("Enhancer Peak Fragment Length \n All Tissues - Roadmap Multiintersect")
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.show()
plt.close()
#########GET 90th PERCENTILE#########
ind=np.arange(len(df_s["num"].unique()))

df_num = df_s["num"]

df_s90 = df_s.loc[df_s["num"]>=df_s["num"].quantile(0.90)] 

df_s90_num = df_s90["num"]

s90_file = "df_s90.bed"

df_s90.to_csv(s90_file, sep = '\t', header = False, index = False)

plt_s, bins, patch= plt.hist(df_num, ind,  alpha = 0.5, label ="All tissue enhancers")
plt_s, bins, patch= plt.hist(df_s90_num, ind, alpha = 0.5,label ="90th% overlapping enhancers")

plt.legend(loc = "upper right")
plt.xlabel("number of overlapping datasets")
plt.ylabel("peak count")

plt.title("Enhancer Overlap in All Tissues \n Roadmap Multiintersect")
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.show()
plt.close()

########## MERGE THE 90TH PERCENTILE #################

merge_s = "%s_df_s90_merge.bed" % now

cmd_s= "mergeBed -i %s -d 100 -c 4 -o collapse -delim ',' > '%s'"% (s90_file, merge_s)

os.system(cmd_s) # 99th = 4785 enhancers, 90th - 38371

#### JACCARD TO MEASURE OVERLAP BETWEEN MERGE_S, MERGE_R ######

data_path = "/Users/sarahfong/Desktop/CAPRA/datasource/"
os.chdir(data_path)

cmd_vs_jaccard = "bedtools jaccard -a %s -b %s" % (villar, merge_s)

#### INTERSECT ROADMAP 90th percentile v. VILLAR ####

villar_s_file = "%s_villar_x_df_s90.bed" %now
villar_s_merge_file = "%s_villar_x_df_s90_merge.bed" % now

cmd_villar_s = "bedtools intersect -a %s -b %s -f 0.5 -e > '%s'" % (villar, s90_file, villar_s_file)
cmd_villar_s_merged = "bedtools intersect -a %s -b %s -f 0.5 -e > '%s'" % (villar,merge_s, villar_s_merge_file)

os.system(cmd_villar_s)
os.system(cmd_villar_s_merged)


sv = pandas.read_table(villar_s_merge_file) # all df_s90 merged fragments
sv.columns = [["chrom", "start", "end", "num"]]
sv["len"] = sv["end"].subtract(sv["start"])

#######IN CASE YOU NEED TO LOOK AT VILLAR V. UNMERGED 90TH% ROADMAP FRAGMENTS #######
"""
vs = pandas.read_table(villar_s_file) # all df_s90 fragments - unmerged
vs.columns = [["chrom", "start", "end", "num"]]
vs["len"] = vs["end"].subtract(vs["start"])
"""
######PLOT LENGTH OF FRAGMENTS 90% MERGED ROADMAP OVERLAPPING WITH VILLAR DATA ########

frag_len = sv["len"]
frag_len_50 = sv["len"].loc[sv["len"] >= 50]
#frag_len_vs = vs["len"]
ind=np.arange(len(sv["len"].unique()))
width = 0.3

#plt_s, bins, patch= plt.hist(frag_len_vs, ind,  alpha = 0.5, label ="frag length villar x 90th - unmerged")
plt_s, bins, patch= plt.hist(frag_len, ind, alpha = 0.5,label =" frag length villar x 90th - merged")
plt_s, bins, patch= plt.hist(frag_len_50, ind,  alpha = 0.5, label ="frag length villar x 90th - merged >=50bp")

plt.legend(loc = "upper right")
plt.xlabel('Overlapping 90th%/Villar enhancer frag length')
plt.ylabel("no.peaks w/ frag length")

plt.title("Method 1 \n Enhancer Peak Fragment Length \n Villar - Roadmap Intersection")
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.show()
plt.close()
