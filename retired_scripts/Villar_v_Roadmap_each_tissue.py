#!/bin/bash/python

#180205 this script has been modified from the 180130 script comparing roadmap hliver active enhancers with villar liver enhancers into a query for  any roadmap active enhancer dataset with evidence of Villar liver enhancers. The purpose of this query is to understand the uniqueness or the ubiquity of Villar hliver enhancers in other roadmap tissues. Identifying active enhancers in Villar's dataset present in the roadmap datasets in any tissue may validate those fragments as bona fide enhancers.

#180206 - The relaxed enhancer definition will be used in attempts to recapitulate Villar's method for identifying enahcers, while the stringent method will be used for orthogonal validation of enhancers with the multiintersect approach used to identify broadly active enhancers. 

#180130 the purpose of this script is to ask how many enhancers identified by Villar histone modifications in human liver overlap enhancers identified in roadmap human livers identified by those same histone modifications.

# Villar Enhancer = H3K27ac+ H3K4me3- with no/ <50% overlap with H3K4me3 ChIP-SEQ dataset.

#'s' = 'stringent' enhancers where H3K27ac modifications absolutely do not overlap H3K4me3 modification fragments.(H3K27ac+/H3K4me3-)
#'r' = 'relaxed' enhancers here H3K27ac modifications do not overlap H3K4me3 modification fragments, or do with less than 50% overlap (H3K27ac+/H3K4me3<50%)

import os, sys
import pandas
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np

now = datetime.date.today()

now = "2018-02-05"
enh_dir = ["/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-22", "/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-23"]

#enh_dir = ["/home/fongsl/roadmap/2018-01-22_stringent/", "/home/fongsl/roadmap/2018-01-23_relaxed/"] # ACCRE directories contains roadmap active enhancer datasets defined by two different definitions.

for path in enh_dir:
    os.chdir(path)
    roadmap_datasets= glob.glob("2018-01-2*.bed")
    date = path.split("/")[-1]

    roadmap_datasets_dict= dict((i.split("_")[6], i) for i in roadmap_datasets) # this is a dictionary of E00*.bed:2018-01-2*.bed

    villar = "HSap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed" # the villar hliver active enhancer dataset.
    
    #perform single intersect to find overlapping villar hliver enhancers in E-xxx_tissue

    tissue_enh_list= []  #count of enhancers in E-xxx_tissue dataset.
    villar_enh_list = [] # count of overlapping villar enhancers in E-xxx_tissue dataset.
    tissue_id_list = [] 
    for tissue, tissue_file in roadmap_datasets_dict.items():
        tissue = tissue.replace(".bed", "")
        
        outfile = "%s_villar_enhancers_x_%s_%s.bed" % (now, tissue, date)
        
        #print("the outfile: ", outfile)
        bed_cmd = "bedtools intersect -a %s -b %s -f 0.5 -e -wa -wb > %s" % (villar, tissue_file, outfile)
        #print ("the bed_cmd", bed_cmd)
        #os.system(bed_cmd)
        
        tissue_enh_count = len(open(tissue_file, "r").readlines()) # counts the number of rows in the roadmap file.
        villar_enh_count = len(open(outfile, "r").readlines()) # counts the number of rows in the overlapping villar roadmap file.

        tissue_enh_list.append(tissue_enh_count) # add entry to dictionary
        villar_enh_list.append(villar_enh_count)
        tissue_id_list.append(tissue)
        

    width = 1.0
    indices = np.arange(len(tissue_enh_list))

    """
    ###### Horizontal Bar Plot #####
    plt.figure(num=None, figsize=(6, 8), dpi=500, facecolor='w', edgecolor='k')

    plt.barh(indices, tissue_enh_list, width, color = 'b', label = 'Roadmap Enhancers')
    
    plt.barh([(i+0.25*width) for i in indices], villar_enh_list, 0.5*width, color = 'r', alpha = 0.5, label = 'Villar-overlap')

    plt.yticks(indices,[i for i in tissue_id_list], fontsize = 6)
    #plt.xticks(indices,[i for i in tissue_id_list], fontsize = 4, rotation = 90)
    plt.xlabel("Count of peaks")
    plt.ylabel("roadmap datasets")
    
    ####### Line plot #######
    plt.figure(num=None, figsize=(10, 6), dpi=500, facecolor='w', edgecolor='k')

    plt.plot(indices, tissue_enh_list, width, color = 'b', label = 'Roadmap Enhancers')
    
    plt.plot([(i+0.25*width) for i in indices], villar_enh_list, 0.5*width, color = 'r', alpha = 0.5, label = 'Villar-overlap')

    #plt.xticks(indices,[i for i in tissue_id_list], fontsize = 4, rotation = 90)
    plt.ylabel("Count of peaks")
    plt.xlabel("roadmap datasets")

    plt.title("villar overlap with roadmap enhancers %s" % date)
    plt.legend()
    #plt.text([i for i in indices], [i for i in villar_enh_list], [i for i in villar_enh_list])
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.savefig("%s_villar_overlap_line.pdf" % date)
    plt.close()

    """
    any_tissue_enhancer=glob.glob("%s*.bed" % now)
    a = ' '.join(str(i) for i in any_tissue_enhancer)
    b = ' '.join(str(i).split("_")[4] for i in any_tissue_enhancer)# a string of the tissue ids
    
    output_multi = "%s_villar_x_any_roadmap_multi.bed" % now
    output_int = "%s_villar_x_any_roadmap_int.bed" % now
    os_multi_cmd = "multiIntersectBed -i %s -sorted -header -names %s > %s" % (a,b, output_multi) # the multiintersect gives count of overlapping records
    os_int_cmd = "Bedtools intersect -a %s -b %s -sorted -header -f 0.5 -e -wa -u > %s" % (villar, a, output_int) # unique intersect tells us how many villar records are found in any tissue.
    os.system(os_multi_cmd)
    os.system(os_int_cmd)
                                                                                            
    #### GET 90TH% AND MERGE ###
    df = pandas.read_table(output_multi)
    df["len"] = df["end"].subtract(df["start"])
    df_90 = df.loc[df["num"]>=df["num"].quantile(0.9)]

    hist_data = df["num"]
    hist90_data = df_90["num"]
    indices = np.arange(len(df["num"].unique()))
    data,bins,patches = plt.hist(hist_data, indices, facecolor = "blue", alpha = 0.75, label = "all tissues")
    data,bins,patches = plt.hist(hist90_data, indices, facecolor = "yellow", alpha = 0.75, label = "90th% tissues")

    
    plt.xlabel("number of overlapping roadmap datasets")
    plt.ylabel("count of peak fragments")
    plt.title("frequency of villar enhancer peaks in roadmap tissues-%s" % date)
    plt.legend()
    plt.savefig("%s_villar_enhancers_overlapping_roadmap.pdf" %date)
    plt.close()

    file_90 = "%s_df_90.bed" % now
    df_90.to_csv(file_90, sep = '\t', header = False, index = False)
    merge = "%s_df_90_merged.bed" % now
    cmd_s= "mergeBed -i %s -d 100 -c 4 -o collapse -delim ',' > '%s'"% (file_90, merge)
    os.system(cmd_s)
    
    # write a bed command to compare the multiIntersectBed with the original Villar data.
    """    
    recovery_file = "villar_x_all_roadmap.bed"
    bed_v_bed_cmd = "bedtools intersect -a %s -b %s -f 0.5 -wao > %s" % (villar, output, recovery_file)
    os.system(bed_v_bed_cmd)
    #this subtraction could also be achieved by a left-outer-join.
    
    villar_unique = "villar_minus_roadmap.bed"
    villar_minus_roadmap = "bedtools intersect -a %s -b %s -v > %s" % (villar, output, villar_unique)
    os.system(villar_minus_roadmap)
    print("done with this file: %s" % path)
    
    """
