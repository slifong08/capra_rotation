 #!/bin/bash/python

#180216 "High Quality" (HQ) villar species ChIP-SEQ datasets were separately analyzed versus Broadly Active Enhancers (BAEs) overlapping the 90th% of enhancers identified in Method 2. We have found that most BAEs do not contain any sequence that aligns with other species, suggesting these enhancers correspond to human specific sequences in regions of the genome recently acquired. To make sure that our finding is not an artifact of noisy sequencing datasets,  we limited our comparison to the high_quality_species datasets.

#180210 METHOD 2 V1 - This script replaces the multiintersection of all roadmap tissues after each being crossed to villar with a pandas query.

#180205 METHOD 2 - this script has been modified from the 180130 script comparing roadmap hliver active enhancers with villar liver enhancers into a query for  any roadmap active enhancer dataset with evidence of Villar liver enhancers. The purpose of this query is to understand the uniqueness or the ubiquity of Villar hliver enhancers in other roadmap tissues. Identifying active enhancers in Villar's dataset present in the roadmap datasets in any tissue may validate those fragments as bona fide enhancers.

#180206 - The relaxed enhancer definition should  be used to recapitulate Villar's method for identifying enhancers in the future. For now, I will use the stringent method for orthogonal validation of enhancers with the multiintersect approach used to identify broadly active enhancers. 

#180130 the purpose of this script is to ask how many enhancers identified by Villar histone modifications in human liver overlap enhancers identified in roadmap human livers identified by those same histone modifications.

# Villar Enhancer = H3K27ac+ H3K4me3- with no/ <50% overlap with H3K4me3 ChIP-SEQ dataset.

#'s' = 'stringent' enhancers where H3K27ac modifications absolutely do not overlap H3K4me3 modification fragments.(H3K27ac+/H3K4me3-)
#'r' = 'relaxed' enhancers here H3K27ac modifications do not overlap H3K4me3 modification fragments, or do with less than 50% overlap (H3K27ac+/H3K4me3<50%)

import os, sys
import pandas
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
plt.style.use('seaborn-deep')
today = datetime.date.today()

mkdir = "mkdir /Users/sarahfong/Desktop/CAPRA/%s" %today
os.system(mkdir)
now = "2018-02-10"

path = "/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-22"

os.chdir(path)

roadmap_datasets= sorted(glob.glob("2018-01-2*.bed"))

date = path.split("/")[-1]

roadmap_datasets_dict= dict((i.split("_")[6], i) for i in roadmap_datasets) # this is a dictionary of E00*.bed:2018-01-2*.bed

villar = "HSap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed" # the villar hliver active enhancer dataset.

v = pandas.read_table(villar)

v.columns=["v-chr", "v-start", "v-end", "IDs"]

#perform single intersect to find overlapping villar hliver enhancers in E-xxx_tissue

df = pandas.DataFrame(columns= ["tissue","num_roadmap", "num_villar","num_villar_u", "overlap", "overlap_u"]) #empty dataframe to measure the overlap of roadmap_datasets and villar_dataset for each tissue.

for tissue, tissue_file in roadmap_datasets_dict.items():
    tissue = tissue.replace(".bed", "")
    
    outfile = "%s_villar_enhancers_x_%s_%s.bed" % (now, date, tissue)
    outfile_u = "%s_villar_enhancers_u_x_%s_%s.bed" % (now, date, tissue)

    bed_cmd = "bedtools intersect -a %s -b %s -f 0.5 -e -wa -wb > %s" % (villar, tissue_file, outfile)
    #print(bed_cmd)
    #os.system(bed_cmd)
    
    bed_cmd_u = "bedtools intersect -a %s -b %s -f 0.5 -e -u > %s" % (villar, tissue_file, outfile_u)
    #print(bed_cmd_u)
    #os.system(bed_cmd_u)
    
    tissue_enh_count = len(pandas.read_table(tissue_file))
    villar_enh_count = len(pandas.read_table(outfile))
    villar_u_enh_count = len(pandas.read_table(outfile_u))
    overlap = (villar_enh_count/tissue_enh_count)
    overlap_u = (villar_u_enh_count/tissue_enh_count)

    df2 = pandas.DataFrame([[tissue, tissue_enh_count, villar_enh_count, villar_u_enh_count, overlap, overlap_u]], columns= ["tissue","num_roadmap", "num_villar","num_villar_u", "overlap", "overlap_u"])
    df = pandas.concat([df2, df])

width = 0.7 
indices = np.arange(len(df["tissue"]))

df= df.sort_values(by ="num_villar_u", ascending=False)
df.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/Villar_x_Roadmap_df_summary.csv" %today , sep ='\t', index = False)

################## Horizontal Bar Plot VILLAR ENHANCER OVERLAP PER ROADMAP TISSUE  ##############################

plt.figure(num=None, figsize=(10,6), dpi=200, facecolor='w', edgecolor='k')

plt.bar(indices, df["num_roadmap"], width, color = 'k', label = 'Roadmap', alpha = 0.25)

#plt.bar([(i+0.25*width) for i in indices], df["num_villar"], 0.5*width, color = 'r', alpha = 0.5, label = 'Villar-Overlap All')

plt.bar([(i-0.25*width) for i in indices], df["num_villar_u"], 0.5*width, color = 'r', alpha = 0.5, label = 'Villar-Overlap') #Unique Villar Enhancers

plt.xticks(indices,[i for i in df["tissue"]], fontsize = 6, rotation=90)
plt.yticks(fontsize = 10)
plt.ylabel("Num. Enhancer Peaks", fontsize = 10)

plt.xlabel("Roadmap Datasets", fontsize = 10)
plt.title("Abs Num. Overlapping Villar Peaks in Roadmap", fontsize = 10)
plt.legend(fontsize = 10)
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)

plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_villar_overlap_bar-non-h-_abs.pdf" %(today,today))
plt.close()
    
###################### Line plot VILLAR ENHANCER OVERLAP PER ROADMAP TISSUE ###################

plt.figure(num=None, figsize=(8,6), dpi=100, facecolor='w', edgecolor='k')

plt.plot(indices, df["num_roadmap"], width, color = 'k', label = 'Roadmap', alpha = 0.25)
plt.plot(indices, df["num_villar"], width, color = 'b', label = 'Villar-All ')
plt.plot(indices, df["num_villar_u"], width, color = 'r', label = 'Villar-Overlap Unique')


plt.xticks(indices,[i for i in df["tissue"]], fontsize = 6, rotation = 90)
plt.ylabel("Num. Enhancer peaks")
plt.xlabel("Roadmap Datasets")

plt.title("Abs Num. Overlapping Villar Peaks in Roadmap")
plt.legend()

plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_villar_overlap_line.pdf" % (today,today))
#plt.show()
plt.close()

#################### Horizontal Bar Plot FREQUENCY OF VILLAR OVERLAP PER ROADMAP TISSUE ################# #####

df= df.sort_values(by ="overlap_u", ascending = False)
plt.figure(num=None, figsize = (10,4), dpi=200, facecolor= 'w', edgecolor ='k')

plt.bar(indices, df["overlap_u"], width, color = 'g', label = 'Roadmap Enhancers')
#plt.barh(indices, df["overlap_u"], width, color = 'g', label = 'Villar Enhancers')

plt.xticks(indices+0.5*width,[i for i in df["tissue"]], fontsize = 6, rotation = 90)
#plt.yticks(indices,[i for i in df["tissue"]], fontsize = 5)

plt.ylabel("Overlap", fontsize=8)

plt.xlabel("Roadmap Datasets", fontsize=8)
plt.legend(fontsize=8)
plt.title("% Overlapping Unique Villar Enhancers in Roadmap Tissues", fontsize=8)

plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_freq_villar_overlap_in_roadmap_bar.pdf" % (today, today))
plt.close()

############## INTERSECT ALL UNIQUE VILLAR ACTIVE ENHANCERS PER ROADMAP TISSUE TO FIND COMMONLY OVERLAPPING ENHANCERS BETWEEN DATASETS  #########

any_tissue_enhancer=glob.glob("%s_villar_enhancers_u*.bed" % now)

results = v

for tissue in any_tissue_enhancer:

    df= pandas.read_table(tissue)
    
    tissue_id = (tissue.split("_")[6]).split(".")[0]

    df.columns = ["%s-chr" %tissue_id, "%s-start"%tissue_id, "%s-end"%tissue_id, "IDs"]
    df["%s"%tissue_id] = 1

    results = pandas.merge(results, df[["IDs","%s" %tissue_id]], how = "left", on = "IDs")

a = results.columns[4:]
results["sum"] = results[a].sum(axis =1)
#results["sum"] = results["sum"].fillna(0)

b = results["sum"].quantile(0.9) # the zeros do not change the 90th % cut off of overlapping datasets. 
results_90 = results.loc[results["sum"]>= b]
results_90["90_percentile"] = 1 # create a column that marks enhancers in the 90th%. 1 = 90th% enhancer, 0 = enhancer is not in 90th%

results_merged = pandas.merge(results, results_90[["IDs","90_percentile"]], how = "left", on = "IDs")
results_merged["90_percentile"] = results_merged["90_percentile"].fillna(0) # fill in the rest of the 90th% genes with zeros

now = datetime.date.today()
results_trans = results_merged.transpose()
#results_90.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/method2_villar_x_any_roadmap_90th_%s.bed" % (now,now), sep = '\t', index = False, header = False) #save 90th% without villar species merge.

#results_merged[["v-chr","v-start", "v-end"]].to_csv("/Users/sarahfong/Desktop/CAPRA/%s/method2_villar_x_any_roadmap_%s_coordinates.bed" % (now,now), sep = '\t', index = False, header = False) # save dataframe

#results_trans.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/method2_villar_x_any_roadmap_%s.csv" % (now,now), sep = '\t', index = False, header = True) # save transposed dataframe

##################### PLOT 90TH% AS A HISTOGRAM ############
hist_data = results["sum"]
hist90_data = results_90["sum"]
indices = np.arange(results["sum"].max())
data,bins,patches = plt.hist(hist_data, indices, facecolor = "blue", alpha = 0.75, label = "All Enhancers")
data,bins,patches = plt.hist(hist90_data, indices, facecolor = "yellow", alpha = 0.75, label = "90th% Enhancers")

plt.xlabel("Num. Overlapping Roadmap Datasets")
plt.ylabel("Num. Villar Enhancers")
plt.title("Overlapping Villar Enhancers in Roadmap Datasets")
plt.legend()
plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_villar_enhancers_overlapping_roadmap.pdf" %(today, today))
plt.close()

data = [hist_data, hist90_data]
datatype = "Roadmap Villar Overlap"
labels = ["all enh", "90th% enh"]
#histogram(data, indices, datatype, labels, 10)

############# COMPARE TO VILLAR ANALYSIS OF ACTIVE/ALIGNABLE ENHANCERS###########
path = "/Users/sarahfong/Desktop/CAPRA/datasource/"
os.chdir(path)

#'vsal' = 'Villar Species Activity/ aLignment' dataframe
vsal = pandas.read_table("Hsap_Enhancers_conservationInOthers.bed", sep = '\t')

hq_species =['Mmul', 'Cjac', 'Mmus', 'Rnor', 'Ocun', 'Btau', 'Sscr', 'Cfam', 'Fcat']

species = vsal.columns[4:22]

#sum activity across species first
vsal[species] = vsal[species].replace("-",0) # replace all alignable, not active enhancers with '0' to sum enhancer activity
vsal[species] = vsal[species].replace({'H3': 1}, regex = True) # replace all the active enhancers with '1' to sum 
vsal["sum_act_sp"] = vsal[species].sum(axis=1)
vsal["sum_hq_act_sp"] = vsal[hq_species].sum(axis = 1)

#now,sum alignability across species second
vsal[species] = vsal[species].replace(0,1) #now, replace all alignable, non-active enhancers with '1' to sum alignability

vsal["sum_aln_sp"] = vsal[species].sum(axis=1)
vsal["sum_hq_aln_sp"] = vsal[hq_species].sum(axis = 1)

vrs = pandas.merge(results_merged, vsal, how = "left", on= "IDs")

vrs.sort_values(by = "sum")

vrs.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/Villar_Roadmap_Species_vrs_overlap_%s.csv" %(now, now), sep = '\t', index = False, header = True)

short_vrs = vrs[["v-chr", "v-start", "v-end","IDs", "sum","90_percentile", "sum_act_sp", "sum_aln_sp", "sum_hq_act_sp", "sum_hq_aln_sp"]]
short_vrs= short_vrs.sort_values(by = "sum")
short_vrs["length"] = short_vrs["v-end"].subtract(short_vrs["v-start"])

short_vrs = short_vrs.fillna(0)
short_vrs.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/Villar_Roadmap_Species_short_vrs_overlap_%s.csv" %(now, now), sep = '\t', index = False, header = True)

short_vrs_90 = short_vrs.loc[short_vrs["sum"]>= b]
short_vrs_not_90 = short_vrs.loc[short_vrs["sum"]< b]
short_vrs_90.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/short_vrs_90_overlap_%s" %(now, now), sep = '\t', index = False, header = False)

############ LENGTH OF BROADLY ACTIVE ENHANCERS ########

# See vrs_boxplot_length.py

############ HUMAN-SPECIFIC/ SPECIES CONSERVED ####### 
comp = ["sum_act_sp", "sum_aln_sp", "sum_hq_act_sp", "sum_hq_aln_sp"]
for item in comp:
    hspec = short_vrs_90[["v-chr", "v-start", "v-end"]].loc[short_vrs_90['%s' %item] == 0]
    hspec.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/hspec_vrs_90_%s_zero_%s.bed" %(now, item, now), sep = '\t', index = False, header = False)
    consv = short_vrs_90[["v-chr", "v-start", "v-end"]].loc[short_vrs_90['%s'%item] > 0]
    consv.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/consv_vrs_90_%s_greater_than_zero_%s.bed" %(now,item, now), sep = '\t', index = False, header = False)
    hspec.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/hspec_vrs_90_%s_zero_%s.bed" %(now, item, now), sep = '\t', index = False, header = False)
    consv_10 = short_vrs_90[["v-chr", "v-start", "v-end"]].loc[short_vrs_90['%s'%item] >= 10]
    consv_10.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/consv_10_vrs_90_%s_greater_than_zero_%s.bed" %(now,item, now), sep = '\t', index = False, header = False)

############# SPECIES V. TISSUES DATA TO PLOT ##############

# all of the enhancers
sum_roadmap = short_vrs["sum"].loc[short_vrs["sum"]>0] # Villar enhancers overlapping roadmap
sum_act_sp = short_vrs["sum_act_sp"].loc[short_vrs["sum"]>0]
sum_aln_sp = short_vrs["sum_aln_sp"].loc[short_vrs["sum"]>0]
sum_hq_act_sp= short_vrs["sum_hq_act_sp"].loc[short_vrs["sum"]>0]
sum_hq_aln_sp= short_vrs["sum_hq_aln_sp"].loc[short_vrs["sum"]>0]

#90th% of enhancers
sum_roadmap_90 = short_vrs_90["sum"].loc[short_vrs_90["sum"]>0] # Villar enhancers overlapping roadmap
sum_act_sp_90 = short_vrs_90["sum_act_sp"].loc[short_vrs_90["sum"]>0]
sum_aln_sp_90 = short_vrs_90["sum_aln_sp"].loc[short_vrs_90["sum"]>0]
sum_hq_act_sp_90= short_vrs_90["sum_hq_act_sp"].loc[short_vrs_90["sum"]>0]
sum_hq_aln_sp_90= short_vrs_90["sum_hq_aln_sp"].loc[short_vrs_90["sum"]>0]

# enhancers only found in Villar
sum_roadmap_v = short_vrs["sum"].loc[short_vrs["sum"]==0] # Villar enhancers overlapping roadmap
sum_act_sp_v = short_vrs["sum_act_sp"].loc[short_vrs["sum"]==0]
sum_aln_sp_v= short_vrs["sum_aln_sp"].loc[short_vrs["sum"]==0]
sum_hq_act_sp_v= short_vrs["sum_hq_act_sp"].loc[short_vrs["sum"]==0]
sum_hq_aln_sp_v= short_vrs["sum_hq_aln_sp"].loc[short_vrs["sum"]==0]

#### h-specific v. consv ACTIVITY dataframes ####
hspec = short_vrs.loc[short_vrs["sum_act_sp"] == 0]
consv = short_vrs.loc[short_vrs["sum_act_sp"] > 0]

hspec_90 = short_vrs_90.loc[short_vrs_90["sum_act_sp"] == 0]
consv_90 = short_vrs_90.loc[short_vrs_90["sum_act_sp"] > 0]


data_list_all = [hspec["length"], consv["length"], hspec_90["length"], consv_90["length"]]
data_list_90 = [hspec_90["length"], consv_90["length"]]
bins = 3000 
labels = ["hspec enh", "consv enh", "hspec_90 enh", "consv_90 enh"]
datatype = "length"
axis = short_vrs["length"].max()
#histogram(data, indices, datatype, labels, axis)

#data, bins, patches = plt.hist(data_list_all, 100, alpha = 0.5, normed = False,  label = labels)
data, bins, patches = plt.hist(data_list_90, 100, alpha = 0.5, normed = False,  label = labels)
plt.boxplot(data_list_all, labels = labels)
#plt.boxplot(data_list_90)
#plt.xscale( "log")
#plt.legend()
plt.ylabel("length (nt)")
plt.yscale("log")
#plt.ylabel("Num Enhancers")
plt.title("length of enhancers")

oneway_f, oneway_p = stats.f_oneway(data_list_all[0],data_list_all[1],data_list_all[2],data_list_all[3])
########### Plot the distribution of # species active enh  v. # of tissues #########

plt.figure(num=None, figsize = (8,6), dpi=200, facecolor= 'w', edgecolor ='k')
plt.scatter(sum_act_sp,sum_roadmap, c= "blue", alpha= 0.5, label = "Villar Active Enh Overlapping All Roadmap")
plt.scatter(sum_act_sp_90,sum_roadmap_90, c = "yellow", alpha = 0.5,label = "Villar Active Enh Overlapping 90% Roadmap")
plt.scatter(sum_act_sp_v, sum_roadmap_v, c= "red", alpha = 0.5, label =  "Villar Active Enh Not Overlapping Roadmap")

plt.scatter(sum_hq_act_sp, sum_roadmap, c= "green", alpha= 0.75, label = "HQ-Villar Active Enh Overlapping All Roadmap")
plt.scatter (sum_hq_act_sp_90, sum_roadmap_90, c = "orange", alpha = 0.75,label = "HQ-Villar Active Enh Overlapping 90% Roadmap")
plt.scatter(sum_hq_act_sp_v,sum_roadmap_v, c= "purple", alpha= 0.75, label = "HQ-Villar Active Enh Overlapping All Roadmap")
plt.ylabel("Num. Overlapping Roadmap Datasets")

#plt.ylim(ymax = 20)
plt.xticks(np.arange(0, 19, 2))
plt.xlabel("Num. Species - Active Enh")
plt.legend()
plt.title(" Num. Species Active Enh  \n x Num. Overlapping Roadmap Active Enhancers")

plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_overlap_roadmap_x_act_enh_sp_%s.pdf" % (now,now,now))
plt.close()
############# Heat map? ##################

plt.figure(num=None, figsize = (8,6), dpi=200, facecolor= 'w', edgecolor ='k')
plt.scatter(sum_aln_sp,sum_roadmap, c= "blue", alpha= 0.5, label = "Villar Active Enh Overlapping All Roadmap")
plt.scatter(sum_aln_sp_90,sum_roadmap_90, c = "yellow", alpha = 0.5,label = "Villar Active Enh Overlapping 90% Roadmap")
plt.scatter(sum_aln_sp_v, sum_roadmap_v, c= "red", alpha = 0.5, label =  "Villar Active Enh Not Overlapping Roadmap")

plt.scatter(sum_hq_aln_sp, sum_roadmap, c= "green", alpha= 0.75, label = "HQ-Villar Active Enh Overlapping All Roadmap")
plt.scatter (sum_hq_aln_sp_90, sum_roadmap_90, c = "orange", alpha = 0.75,label = "HQ-Villar Active Enh Overlapping 90% Roadmap")
plt.scatter(sum_hq_aln_sp_v,sum_roadmap_v, c= "purple", alpha= 0.75, label = "HQ-Villar Active Enh Overlapping All Roadmap")
plt.ylabel("Num. Overlapping Roadmap Datasets")

#plt.ylim(ymax = 20)
plt.xticks(np.arange(0, 19, 2))
plt.xlabel("Num. Species - Alignable Enh")
plt.legend()
plt.title(" Num. Species Alignable Enh  \n x Num. Overlapping Roadmap Active Enhancers")

plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_overlap_roadmap_x_aln_enh_sp_%s.pdf" % (now,now,now))
plt.close()


############# PLOT HISTOGRAMs ############

bins = np.arange(short_vrs["sum_aln_sp"].max()+1)
bins_hq = np.arange(short_vrs["sum_hq_aln_sp"].max()+1)

data_aln = [sum_aln_sp, sum_aln_sp_90]
data_aln_hq = [sum_hq_aln_sp, sum_hq_aln_sp_90]
data_act = [sum_act_sp, sum_act_sp_90]
data_act_hq = [sum_hq_act_sp, sum_hq_act_sp_90] # a 
labels = ["All hVillar", "90th hVillar x Roadmap"] # list of the labels of the data


def histogram(data, bins, datatype, labels, axis):
    plt.hist(data, bins, alpha = 0.7, normed= False, label = labels)
    plt.xticks(np.arange(0, len(bins)+1, axis))
    #plt.axhline((data.max() for i in bins, color='y', linestyle='dashed', linewidth=1)
    plt.xlabel("Num. Species %s Enhancers" % datatype)
    plt.ylabel("Num. hVillar Enhancers")
    plt.title("Abs. Number Broadly Active Enhancers \n %s In Other All Species" % datatype)
    plt.legend()

    #plt.show()
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_hist_abs_%s.pdf" %(now,now, datatype))
    plt.close()

    plt.hist(data, bins, alpha = 0.7, normed= True, label = labels)
    plt.xticks(np.arange(0, len(bins+1), axis))             
    
    #plt.axhline((data.max() for i in bins, color='y', linestyle='dashed', linewidth=1)
    plt.xlabel("Num. Species %s Enhancers"%datatype)
    plt.ylabel("Freq. hVillar Enhancers")
    plt.title("Frequencey Of Broadly Active Enhancers \n %s In Other All Species" % datatype)
    plt.legend()

    #plt.show()
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_hist_normed_%s.pdf" %(now,now, datatype))
    plt.close()

histogram(data_aln, bins,"Alignable", labels,1)
histogram(data_aln_hq,bins_hq,"Alignment_HQ", labels,1)

histogram(data_act,bins,"Active", labels, 1)
histogram(data_act_hq,bins_hq,"Active_HQ", labels, 1)

