#!/bin/bash/python

import pandas
import matplotlib.pyplot as plt
import numpy
import os

path = "/home/fongsl/roadmap/2018-01-22/"
os.chdir(path)

df = pandas.read_table("small_all_tissue_Hsap.csv")
df.sort_values(by=['num'])
df["length"] = df["end"].sub(df["start"]) # measure the length of the sequences

###############PLOT BY NUMBER OF DATASETS##################
counts = df["num"]
unique_counts = df["num"].unique()

num_bins = len(unique_counts)

plt.xlabel("Number of datasets")
plt.ylabel("Hit Count")
plt.title("Count of Enhancers Overlapping Datasets - Roadmap Encode")
counts, num_bins, patches = plt.hist(counts, num_bins, facecolor='blue', alpha=0.5)
plt.savefig("no_datasets_v_hitcount.pdf")

############### hits v. length##################
fig, ax = plt.subplots()
frg_lg = df[["length", "num"]].loc[df["chrom"]== "chr19"]
x = frg_lg["num"]
y = frg_lg["length"]
#fit = numpy.polyfit(x, y, deg=1)
#ax.plot(x, fit[0] * x + fit[1], color='red')
ax.scatter(x,y)
ax.set_yscale('log', basey=2) # do not fit line for log2 y-scaling
plt.xlabel("Number of datasets")
plt.ylabel("length of consensus fragment")
plt.title("number of hits v. fragment length of hit")
plt.savefig("num_datasets_v_consensus.pdf")
