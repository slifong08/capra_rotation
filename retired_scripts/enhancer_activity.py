#!/bin/bash/python

#180131

# the 1st purpose of this script is to plot the activity of enhancer fragments by dataset.

# the 2nd purpose of this script is to plot activity of enhancer fragments by chromosome locus.

# dataset = "2018-01-22_small_all_tissue.csv" is a dataset containing all enhancer fragments found in 98 datasets of the roadmap consolidated epigenomes. Active enhancer in this dataset is stringently defined as having an H3K27ac histone modification, but without overlapping  H3K4me3 histone modification fragment at all.

import pandas
import matplotlib.pyplot as plt
import os
from pylab import plot, savefig, xlim, figure, hold, ylim, boxplot, setp, axes, title, xlabel, ylabel
#open the roadmap E108 hliver enhancer file

roadmap_liv_path = "/Users/sarahfong/Desktop/CAPRA/datasource"

os.chdir(roadmap_liv_path)

roadmap_hliv_infile = "2018-01-22_Hsap_H3K27ac_plus_H3K4me3_minus_E108.bed"

df_rhliv = pandas.read_table(roadmap_hliv_infile)
df_rhliv.columns = ["chrom", "start", "end", "num"]
df_rhliv["chrom_2"] =df_rhliv["chrom"].str.replace('chr','') # remove 'chr'
df_rhliv["length"] = df_rhliv["end"].subtract(df_rhliv["start"])


#open the villar hliver enhancer file

#villar_infile = "Hsap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed"

villar_infile ="Hsap_H3K27ac_humanspEnhancers.bed" # this file 100% overlaps the fragments in  "Hsap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed", contains conservation annotations.

df_vhliv = pandas.read_table(villar_infile)
df_vhliv.columns = ["chrom", "start", "end", "notes"]
df_vhliv["chrom_2"] =df_vhliv["chrom"].str.replace('chr','') # remove 'chr'
df_vhliv["length"] = df_vhliv["end"].subtract(df_vhliv["start"])

#open the multiintersect bed file

multi_path = "/Users/sarahfong/Desktop/CAPRA/2018-01-22"
multi_infile = "2018-01-22_small_all_tissue.csv"

os.chdir(multi_path)

df = pandas.read_table(multi_infile)

df["chrom_2"] =df["chrom"].str.replace('chr','') # remove 'chr'
df["length"] = df["end"].subtract(df["start"])

hliver = "E108"

# make a dataframe containing rows with liver enhancer annotation

dfliv = df.loc[df["list"].str.contains(hliver)]

dfliv["length"] = dfliv["end"].subtract(dfliv["start"]) # find the length of the fragments

###### Histogram of Length of Fragments #####

A= df_rhliv["length"]
B= df_vhliv["length"]
C= df["length"]
D = dfliv["length"]
data = [A, B, C, D]

binn = int(df_rhliv["length"].max()/10)
data, bins, patches = plt.hist(A, binn, facecolor='blue', alpha=0.75)
data, bins, patches = plt.hist(B, binn, facecolor='green', alpha=0.75)

#data, bins, patches = plt.hist(C, binn, facecolor='yellow', alpha=0.75)
data, bins, patches = plt.hist(D, binn, facecolor='red', alpha=0.75)


plt.xlabel("frag length")
plt.ylabel("count")
plt.title("length of frag \n hliver s_enhancers \n Roadmap 98 multiintersect dataset")

##### Boxplot of fragment lengths across datasets ####

fig, ax = plt.subplots()

plt.boxplot(data)
ax.set_yscale("log")
ax.set_title("size of fragments")
ax.set_xticklabels(["roadmap_hliv", "villar_hliv","multiint-hliver", "multiint-all-frags"])
plt.ylabel("frag size - bp")
