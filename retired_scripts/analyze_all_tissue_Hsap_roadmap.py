#bin/bash/env python
#1801223
#sarahfong

#the purpose of this script is to analyze the results of a multiintersect bed analysis of all human enhancers H3K27ac+ H3K4me3- found in roadmap dataset
import os
import pandas

path = "/home/fongsl/roadmap/2018-01-22"
os.chdir(path)

df = pandas.read_table("2018-01-22_all_tissue_Hsap_H3K27ac_plus_H3K4me3_minus.bed", sep='\t')
#df = pandas.read_csv("2018-01-23_all_tissue_Hsap_H3K27ac_plus_H3K4me3_minus.csv", sep ='\t')

small_df = df[["chrom", "start", "end", "num", "list"]]
small_df.to_csv("small_all_tissue_Hsap.csv", sep='\t')

#conserved = small_df.loc[small_df["num"] >60]
#conserved.to_csv("2018-01-23_greater_than_60_small_all_tissue_Hsap.csv", sep='\t')
