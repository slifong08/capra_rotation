#180221 Make enhancer universe as background file for GREAT GO-annotation query. This script will take active enhancers found in Roadmap tissue datasets (here on referred to as 'tissues') and compile them into one giant enhancer universe.  My strategy will be to make a union of all roadmap tissue enhancer datasets. The definition of an enhancer for this enhancer universe will be stringently based on the definition that no H3K27ac mark overlap any H3K4me3 peak. I did the initial query to identify these active enhancers in each tissue on 180122.


import os, sys, random
import pandas
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np

now = datetime.date.today()
now_dir = "/Users/sarahfong/Desktop/CAPRA/%s" %now
mkdir = "mkdir %s" %now_dir
os.system(mkdir)

path = "/Users/sarahfong/Desktop/CAPRA/datasource/roadmap/2018-01-22"

os.chdir(path)

roadmap_datasets= sorted(glob.glob("2018-01-2*.bed"))

date = path.split("/")[-1]
    
roadmap_datasets_dict= dict((i.split("_")[6], i) for i in roadmap_datasets) # this is a dictionary of E00*.bed:2018-01-2*.bed

def get_tissue(file_name):
    for key, value in roadmap_datasets_dict.items():
        if value == file_name:
            return key
val = 0
for item in roadmap_datasets:
    if val = 0:
        df = pandas.read_table(roadmap_datasets[val])
        a=get_tissue(roadmap_datasets[val])
        df.columns=["chr", "start", "end", "%s-counts" %a]
        val = val +1
    else:
        a=get_tissue(roadmap_datasets[val])
        df_temp = pandas.read_table(roadmap_datasets[val])
        df_temp.columns=["chr", "start", "end", "%s-counts" %a]
        df = df.merge(df, df_temp["%s-counts" %a], how = both, on = ["chr", "start", "end"))
        val = val + 1

outfile = "Roadmap_Enhancer_Universe_%s.csv" %now
df.to_csv(outfile, headers = True, index = False)
