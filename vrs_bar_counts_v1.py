# this script is to evaluate all of the villar x roadmap x species sequence alignments without having to run the entire method2_v1,2,3 scripts

import os, sys
import pandas
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
plt.style.use('seaborn-deep')
today = datetime.date.today()
now = today

###### HISTOGRAMs FUNCTION ##########3
def histogram(data, bins, datatype, labels):
    data, bins, bars = plt.hist(data, bins, alpha = 0.7, normed= False, label = labels)

    plt.xticks(np.arange(0, len(bins), 1))
   
    plt.xlabel("Num. Species \n %s Enhancers" % datatype)
    plt.ylabel("Num. hVillar Enhancers")
    plt.title("Abs. Number Broadly Active Enhancers \n In Other Species -  %s" % datatype)
    plt.legend()

    for i in data:
        val = 0
        for j in i:
            plt.annotate(str(j).split('.')[0], xy =(val+0.5, j+10))
            val = val +1
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_hist_abs_%s.pdf" %(now,now, datatype))
    plt.close()

    # This dataframe is saves as "short_vrs" in the method2_v3.py script

short_vrs = pandas.read_csv("/Users/sarahfong/Desktop/CAPRA/2018-02-27/short_vrs_overlap_2018-02-27.csv", sep = '\t')
vrs = short_vrs

q25= int(vrs["sum"].quantile(0.25))
q50 = int(vrs["sum"].quantile(0.50))
q75 = int(vrs["sum"].quantile(0.75))
q90 = int(vrs["sum"].quantile(0.90))
 # 'a' = 'all'
 # 'h' = 'human' specific enhancers
 # 'c' = 'conserved' enhancers
 # 'd' = 'dataset'

 #'act' = active
 #'aln' = alignable
short_vrs_90 = short_vrs.loc[short_vrs["90_percentile"] >0]
dtype = ['sum_act_sp', 'sum_aln_sp', 'sum_hq_act_sp', 'sum_hq_aln_sp']

# all of the enhancers
for item in dtype:
    bins = np.arange(short_vrs[item].max()+1)

    if item == 'sum_act_sp':
        title = "Active"
    elif item == 'sum_aln_sp':
        title =  "Alignable"
    elif item == 'sum_hq_act_sp':
        title =  "Active- HQ"
    elif item == 'sum_hq_aln_sp':
        title =  "Alignable - HQ"
    else:
        title = "Alignable, not Active"
    # Villar enhancers overlapping roadmap
    sum_roadmap = short_vrs["sum"].loc[short_vrs["sum"]>0] # not plotted, but loaded in case you want to plot this data
    sum_item = short_vrs[item].loc[short_vrs["sum"]>0] # PLOTTED
    
    #90th% of enhancers
    sum_roadmap_90 = short_vrs_90["sum"].loc[short_vrs_90["sum"]>0] # not plotted, but loaded in case you want to plot this data
    sum_item_90 =  short_vrs_90[item].loc[short_vrs_90["sum"]>0] # PLOTTED
    
    # Villar enhancers only found in Villar
    sum_roadmap_v = short_vrs["sum"].loc[short_vrs["sum"]==0] # not plotted, but loaded in case you want to plot this data.
    sum_item_v = short_vrs[item].loc[short_vrs["sum"]==0] # not plotted, but loaded in case you want to plot this data. 

    #### h-specific v. consv ACTIVITY dataframes #### ### in case you want to plot this data.
    hspec = short_vrs.loc[short_vrs[item] == 0]
    consv = short_vrs.loc[short_vrs[item] > 0]

    hspec_90 = short_vrs_90.loc[short_vrs_90[item] == 0]
    consv_90 = short_vrs_90.loc[short_vrs_90[item] > 0]

    

    ########### Plot the distribution of # species active enh  v. # of tissues #########
    """
    plt.figure(num=None, figsize = (8,6), dpi=200, facecolor= 'w', edgecolor ='k')
    plt.scatter(sum_item, sum_roadmap, c= "blue", alpha= 0.5, label = "Villar %s  Overlapping All Roadmap" %x)
    plt.scatter(sum_item_90, sum_roadmap_90, c = "yellow", alpha = 0.5, label= "Villar %s  Overlapping All Roadmap" %x)
    plt.scatter(sum_item_v, sum_roadmap_v, c= "red", alpha = 0.5, label = "Villar %s  Overlapping All Roadmap" %x)

    plt.ylabel("Num. Overlapping Roadmap Datasets")
    plt.xticks(np.arange(0, len(bins)+1, 1))
    #plt.ylim(ymax = 20)

    plt.xlabel("Num. Species \n- %s" % x)
    plt.legend()
    plt.title(" Num. Species %s Enh  \n x Num. Overlapping Roadmap Active Enhancers" % x)

    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_method2_overlap_roadmap_x_act_%s.pdf" % (now,now,item))
    plt.close()
    """
    ############# PLOT HISTOGRAMs ############

    data = [sum_item, sum_item_90]

    labels = ["All hVillar", "90th hVillar x Roadmap"] # list of the labels of the data

    histogram(data, bins, title, labels)
    

