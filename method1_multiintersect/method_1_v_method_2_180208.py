#180208 The purpose of this script is to identify broadly active enhancers that have been identified by Method 1 and Method 2

#Method 1 first queries all roadmap datasets for commonly active enhancer fragment peaks via MultiIntersect, merges that 90th% of peaks, then crosses references those peaks with the villar dataset.

# Method 2 first compares active Villar peaks to each roadmap tissue, then compares the overlap between each villar/tissue peaks to each other. 

import os
import datetime as dt
import pandas
import matplotlib.pyplot as plt

now = dt.date.today()

path = "/Users/sarahfong/Desktop/CAPRA/"
os.chdir(path)

cmd = "mkdir %s" % now
os.system(cmd)

new_path = path+"%s/" % now
os.chdir(new_path)

villar = "HSap_H3K4me3.H3K27Ac_overlap_H3K27Aconly.bed"

method_2_merged = "method2_villar_x_any_roadmap_90th_2018-02-12.bed" #the resulting bed file from method 2 using Vx3illar active enhancers as currency to query each roadmap tissue for overlap. Then, multi-intersects fragments. Merged enhancers have at max 100bp distances between fragment peaks

short_method_2_merged = "short_%s" % method_2_merged

df = pandas.read_table(method_2_merged)
a = df[['chr20', '1355959', '1359701', 'hsaH3K27Ac23394',]]
a.columns = ["chr", "start", "end", "id"]
a.to_csv(short_method_2_merged, sep ='\t', index = False, header = False)
a_set= set(a["id"]) # unique IDS found in method2_villar_x_any_roadmap_90th_2018

method_1_merged = "2018-02-07_method1_villar_x_df_s90_merge.bed"
df = pandas.read_table(method_1_merged)
df.columns=["chr", "start", "end", "id"]
b_set=set(df["id"]) # unique IDs found in method2_villar_x_any_roadmap_90th_2018

dif_a= a_set.difference(b_set) # Method 2 not in method 1
dif_b= b_set.difference(a_set) # Method 1 not in method 2

plt.venn2(subsets = (len(dif_a), len(dif_b), len(set_a.difference(dif_a))), set_labels = ('Method2', 'Method1')) 

def merge(dataset_1, dataset_2):
    
    sort_1= "bedtools sort -i %s > sorted_%s" % (dataset_1, dataset_1)
    os.system(sort_1)
    dataset_1 = ("sorted_%s" % dataset_1)
    print(dataset_1)
    
    sort_2= "bedtools sort -i %s > sorted_%s" % (dataset_2, dataset_2)
    os.system(sort_2)
    dataset_2 = ("sorted_%s" % dataset_2)
    print(dataset_2)

    outfile_int = "%s_results_intersect_sorted.bed" % now
    print(outfile_int)
    cmd_int = "bedtools intersect -a %s -b %s -f 0.5 -e -sorted -wao > %s" % (dataset_1, dataset_2, outfile_int)
    os.system(cmd_int)

    cmd_int = "bedtools intersect -a %s -b %s -f 0.5 -e -sorted -u > u_%s" % (dataset_1, dataset_2, outfile_int)
    os.system(cmd_int)
    
    outfile_jac =  "%s_results_jaccard" % now
    cmd_jaccard = "bedtools jaccard  -a %s -b %s -f 0.5 -e -u -sorted > %s" % (dataset_1, dataset_2, outfile_jac)
    a=os.system(cmd_jaccard)
    return(a)

os.chdir("%s" % now)
merge(method_1_merged, short_method_2_merged)

