<<<<<<< HEAD
#!/usr/bin/env python

=======
#!/usr/bin/python


#sarahfong

#180207 - The MultiIntersect command in step 3 has been relaxed. Originally, we evaluated only reciprocal overlapping peak fragments, which resulted in very few and small peaks calling among all fragments. To relax this, I will change the command from multiIntersectBed -i <<files>> -f 0.5 -r to multiIntersectBed -i <<files>> -f 0.5 -e, which will include the fragment if there is 50% overlap of either fragment. This accomodates overlapping very large peaks with very small peaks.

#180123 - There are two methods in step 2 for subtracting overlapping H3K27ac and H3K4me3 ChIP-SEQ fragments.

#Stringent requirements for calling an enhancer include (1) only non-overlapping H3K27ac+ H3K4me3- fragments.

#Relaxed requirements for calling an enhancer include (1) non-overlapping H3K27ac+ H3K4me3- fragments (-A command) or (2) fragments with 50% overlap of -a and -b in order to subtract one region from another.

# note about Relaxed requirements - This method of using bedtools is based off Mary Lauren's analysis of the roadmap encode dataset looking for histone modifications of enhancer regions that do not contain promoter histone modifications. This is also the method used in Villar 2015 to call enhancers. 

#180118 - the purpose of this script is to iterate through each ENCODE tissue (e.g. E034, E035, etc.) to find any human enhancers. I  define an enhancer as H3K27ac+ and H3K4me3-. H3K4me3- also marks active promoters.


#step 1-  I will glob histone modification files needed to define active enhancers

#step 2-  I will iterate through each tissue ID file using  bedtools to find non-overlapping ChIP-seq regions 
#         I will write a bed file for 
#             Active enhancers: All H3k27ac marks non-overlapping w/ H3k4me3 found in that tissue. I.e. H3k27ac+/H3k4me3-

#step 3-  I will then multiintersect the resultant enhancer files for all tissues. This will be run with the script "roadmap_find_common_enhancer.py
##############################

import os
import sys
import glob
import datetime

import pandas
=======



now = str(datetime.date.today())
"""
#path = "/dors/capra_lab/data/roadmap_epigenomics/consolidated/gappedPeak"
#os.chdir(path)

#step 1- find matching H3k27ac and H3k4me3 datasets per tissue_id

h3k27ac = glob.glob("*H3K27ac*.bed")
h3k4me3 = glob.glob("*H3K4me3*.bed")

h3k27ac_dict = dict((name.split("-")[0], name) for name in h3k27ac)
h3k4me3_dict = dict((name.split("-")[0], name) for name in h3k4me3)

matched_dataset=list(key for key in h3k27ac_dict.keys() if key in h3k4me3_dict.keys())

#step 2- use bedtools to iterate through matching datasets and collect active enhancers, promoters, and H3k27ac-/H3k4me3+ for any tissue.

new_dir = "mkdir %s" % now

os.system(new_dir)

for tissue_id in matched_dataset:

    h3k27ac_val = h3k27ac_dict[tissue_id] # key for h3k27ac.bed filename
    h3k4me3_val = h3k4me3_dict[tissue_id] # key for h3k4me3.bed filename

    output_file = "%s_Hsap_H3K27ac_plus_H3K4me3_minus_%s.bed" % (now, tissue_id)


    #os_cmd = "bedtools intersect -a %s -b %s -sorted -v > '%s'" % (h3k27ac_val, h3k4me3_val, output_file) #stringent enhancer definition. 180122 stringent analysis.

    os_cmd = "bedtools subtract -A -f 0.5 -r -a %s -b %s -sorted > '%s'"% (h3k27ac_val, h3k4me3_val, output_file) #relaxed enhancer definition. 180123 relaxed analysis.

    #os_cmd = "bedtools intersect -a %s -b %s -sorted -v > '%s'" % (h3k27ac_val, h3k4me3_val, output_file) #reports only non-overlapping intervals. 180122 - stringent analysis.

    os_cmd = "bedtools subtract -A -f 0.5 -r -a %s -b %s -sorted > '%s'"% (h3k27ac_val, h3k4me3_val, output_file) #reports overlapping intervals with less than 50% overlap and non-overlapping intervals. 180123 -  relaxed analysis.

#    print("step2 forloop OS_CMD", os_cmd)

    os.system(os_cmd)

#step 3- perform a multi-intersect of the resultant encode enhancer files for all tissues to find commonly active mutations
move_files = "mv *%s*.bed /home/fongsl/roadmap/%s" % (now, now)
os.system(move_files)
"""

now = "2018-01-22"

results_path = "/home/fongsl/roadmap/%s" % now
os.chdir(results_path)

x = "%s*" % now 
any_tissue_enhancer = glob.glob(x) # glob together all the tissues

a = ' '.join(str(i) for i in any_tissue_enhancer)
b = ' '.join(str(i).split("_")[6] for i in any_tissue_enhancer)# a string of the tissue ids

output_handle = "%s_all_tissue_Hsap_H3K27ac_plus_H3K4me3_minus.bed" % now
output_handle_180207 = "180207_all_tissue_Hsap_H3K27ac_plus_H3K4me3_minus.bed"

#os_cmd = "multiIntersectBed -i %s -f 0.5 -r -sorted -header -names %s > %s" % (a,b, output_handle) # used before 180207
os_cmd = "multiIntersectBed -i %s -f 0.5 -e -sorted -header -names %s > %s" % (a,b, output_handle_180207) # used after 180207
#print(os_cmd)
os.system(os_cmd)



