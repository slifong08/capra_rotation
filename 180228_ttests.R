setwd("/Users/sarahfong/Desktop/CAPRA/2018-02-27")
data <- read.csv("Villar_Roadmap_Species_vrs_overlap_2018-02-27.csv", sep ='\t') # this is a csv generated from the method2_v4.py script.
shdata<- read.csv("Villar_Roadmap_Species_short_vrs_overlap_2018-02-27.csv", sep = '\t', header=T)

roadmap <- shdata$sum
q90= quantile(roadmap,c(0.90))
q75= quantile(roadmap,c(0.75))
q50= quantile(roadmap,c(0.50))
q25 = quantile(roadmap, c(0.25))
q0 = quantile(roadmap, c(0))
hspec_act <- shdata[ which(shdata$sum_act_sp ==0),]
hspec_aln <- shdata[ which(shdata$sum_aln_sp ==0),]
con_act <- shdata[ which(shdata$sum_act_sp >0),]
con_aln <- shdata[ which(shdata$sum_aln_sp >0),]

hspec_act90 <- shdata[ which(shdata$sum_act_sp ==0 & shdata$sum>=q90),]

#compare alignability of enhancers with the activity of the enhancers across roadmap datasets.
hspec_aln90 <-shdata[ which(shdata$sum_aln_sp ==0 & shdata$sum>=q90),]
hspec_aln75 <-shdata[ which(shdata$sum_aln_sp ==0 & shdata$sum<q90 & shdata$sum>=q75),]
hspec_aln50 <-shdata[ which(shdata$sum_aln_sp ==0 & shdata$sum<q75 & shdata$sum>=q50),]
hspec_aln25 <-shdata[ which(shdata$sum_aln_sp ==0 & shdata$sum<q50 & shdata$sum>=q25),] # there will be villar enhancers that are not alignable to any roadmap dataset. These will not be included here. 
hspec_aln0 <-shdata[ which(shdata$sum_aln_sp ==0 & shdata$sum<q25 & shdata$sum>= q0),]

con_aln90 <-shdata[ which(shdata$sum_aln_sp >0 & shdata$sum>=q90),]
con_aln75 <-shdata[ which(shdata$sum_aln_sp >0 & shdata$sum<q90 & shdata$sum>=q75),]
con_aln50 <-shdata[ which(shdata$sum_aln_sp >0 & shdata$sum<q75 & shdata$sum>=q50),]
con_aln25 <-shdata[ which(shdata$sum_aln_sp >0 & shdata$sum<q50 & shdata$sum>=q25),]# there will be villar enhancers that are not alignable to any roadmap dataset. These will not be included here. 
con_aln0 <-shdata[ which(shdata$sum_aln_sp >0 & shdata$sum<q25 & shdata$sum>= q0),]

#Are the differences in Length significant between conserved and human specific overlapping pairs?
t.test(hspec_aln90$length, con_aln90$length)
t.test(hspec_aln75$length, con_aln75$length)
t.test(hspec_aln$length, con_aln$length)

fit <- aov(length~ con_aln90, data = shdata)
#### Sanity check - does the number of conserved active enhancers total up to Fish's result?
#### Fish reported 285 conserved active enhancers across 10 HQ datasets from Villar. 
####Do we also observe this? Kinda. We see there are 336 active conserved enhancers between datasets. 
####Maybe she used a black-listed list for this query?

cons_hq_aln <- shdata[ which(shdata$sum_hq_aln_sp ==max(shdata$sum_hq_aln_sp)),] # the number of alignable enhancers in all HQ species
cons_hq_act <- shdata[ which(shdata$sum_hq_act_sp ==max(shdata$sum_hq_aln_sp)),] # the number of active enhancers present in all HQ species
max(shdata$sum_hq_aln_sp)




