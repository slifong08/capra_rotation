# this script is to evaluate all of the villar x roadmap x species sequence alignments without having to run the entire method2_v1,2,3 scripts

import os, sys
import pandas
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
plt.style.use('seaborn-deep')
today = datetime.date.today()

# This dataframe is called "short_vrs" in the method2_v3.py script

vrs = pandas.read_csv("/Users/sarahfong/Desktop/CAPRA/2018-02-24/Villar_Roadmap_Species_short_vrs_overlap_2018-02-24.csv", sep = '\t')


q25= int(vrs["sum"].quantile(0.25))
q50 = int(vrs["sum"].quantile(0.50))
q75 = int(vrs["sum"].quantile(0.75))
q90 = int(vrs["sum"].quantile(0.90))
 # 'a' = 'all'
 # 'h' = 'human' specific enhancers
 # 'c' = 'conserved' enhancers
 # 'd' = 'dataset'

 #'act' = active
 #'aln' = alignable

dtype = ['sum_act_sp', 'sum_aln_sp', 'sum_hq_act_sp', 'sum_hq_aln_sp']
for item in dtype:
    vrs_0 = vrs.loc[vrs["sum"]<q25] # sum = the number of overlapping roadmap tissues
    vrs_0a = vrs_0["length"]
    vrs_0h = vrs_0["length"].loc[vrs_0[item]==0]
    vrs_0c = vrs_0["length"].loc[vrs_0[item] >0]
    vrs_0d = [vrs_0a, vrs_0c, vrs_0h]
    
    vrs_25 = vrs.loc[vrs["sum"]>=q25]
    vrs_25 = vrs_25.loc[vrs_25["sum"]<q50]
    vrs_25a = vrs_25["length"]
    vrs_25h =vrs_25["length"].loc[vrs_25[item]==0]
    vrs_25c = vrs_25["length"].loc[vrs_25[item] >0]
    vrs_25d = [vrs_25a, vrs_25c, vrs_25h]

    vrs_50 = vrs.loc[vrs["sum"]>=q50]
    vrs_50 = vrs_50.loc[vrs_50["sum"]<q75]
    vrs_50a = vrs_50["length"]
    vrs_50h =vrs_50["length"].loc[vrs_50[item]==0]
    vrs_50c = vrs_50["length"].loc[vrs_50[item] >0]
    vrs_50d = [vrs_50a, vrs_50c, vrs_50h]

    vrs_75 = vrs.loc[vrs["sum"]>=q75]
    vrs_75 = vrs_75.loc[vrs_75["sum"]<q90]
    vrs_75a = vrs_75["length"]
    vrs_75h =vrs_75["length"].loc[vrs_75[item]==0]
    vrs_75c = vrs_75["length"].loc[vrs_75[item] >0]
    vrs_75d = [vrs_75a, vrs_75c, vrs_75h]
    
    vrs_90 = vrs.loc[vrs["sum"]>=q90]
    vrs_90a = vrs_90["length"]
    vrs_90h =vrs_90["length"].loc[vrs_90[item]==0]
    vrs_90c = vrs_90["length"].loc[vrs_90[item] >0]
    vrs_90d = [vrs_90a, vrs_90c, vrs_90h]

    all_enh = vrs["length"]

    data_d = [all_enh, vrs_90d, vrs_75d, vrs_50d, vrs_25d, vrs_0d] # all, hu, conserved
    
    data_a = [all_enh, vrs_0a, vrs_25a, vrs_50a, vrs_75a, vrs_90a]
    data_h = [all_enh, vrs_0h, vrs_25h, vrs_50h, vrs_75h, vrs_90h]
    data_c = [all_enh, vrs_0c, vrs_25c, vrs_50c, vrs_75c, vrs_90c]

    data_2 = [all_enh, vrs_0h, vrs_0c, vrs_25h, vrs_25c, vrs_50h, vrs_50c, vrs_75h,vrs_75c, vrs_90h, vrs_90c]

    binn = 50

    ###### LABELS #######

    labels = ["All Enh \n (n=%s)" % len(all_enh), "<25th\n (n=%s)" % len(vrs_0a), "25-50th \n (n=%s)" % len(vrs_25a), "50-75th \n (n=%s)" % len(vrs_50a), "75-90th \n (n=%s)" % len(vrs_75a), "90th \n (n=%s)" % len(vrs_90a)]

    labels_c = ["All Enh \n (n=%s)" % len(all_enh), "<25th\n (n=%s)" % len(vrs_0c), "25-50th \n (n=%s)" % len(vrs_25c), "50-75th \n (n=%s)" % len(vrs_50c), "75-90th \n (n=%s)" % len(vrs_75c), "90th \n (n=%s)" % len(vrs_90c)]

    labels_h = ["All Enh \n (n=%s)" % len(all_enh), "<25th\n (n=%s)" % len(vrs_0h), "25-50th \n (n=%s)" % len(vrs_25h), "50-75th \n (n=%s)" % len(vrs_50h), "75-90th \n (n=%s)" % len(vrs_75h), "90th \n (n=%s)" % len(vrs_90h)]
    
    labels2 = ["All Enh \n (n=%s)" % len(all_enh), "<25th-h \n (n=%s)" % len(vrs_0h),"<25th-c\n (n=%s)" % len(vrs_0c),"25-50th-h \n (n=%s)" % len(vrs_25h),  "25-50th-c \n (n=%s)" % len(vrs_25c), "50-75th-h \n (n=%s)" % len(vrs_50h), "50-75th-c \n (n=%s)" % len(vrs_50c), "75-90th-h \n (n=%s)" % len(vrs_75h),"75-90th-c \n (n=%s)" % len(vrs_75c), "90th-h \n (n=%s)" % len(vrs_90h),"90th-c \n (n=%s)" % len(vrs_90c),]

    if item == 'sum_act_sp':
        x = "Active Enh"
    elif item == 'sum_aln_sp':
        x =  "Alignable Enh"
    elif item == 'sum_hq_act_sp':
        x =  "Active Enh - HQ"
    elif item == 'sum_hq_aln_sp':
        x =  "Alignable Enh - HQ"

    ########### LINE+ HISTOGRAM #########
    val = 0
    for a in data_a:
        ind = val
        data, bins, patches = plt.hist(a, binn, alpha = 0.1, normed = False)
        
        bincenters = 0.5*(bins[1:]+ bins[:-1])
        plt.plot(bincenters,data,'-', label = labels[val])
        
        val = val + 1
    plt.xscale("log")
    
    plt.xlabel("Length of Enhancers (nt)")
    
    plt.ylabel("%s - Count" % x)
    plt.legend()
    plt.title("Length of Enhancers v. Count Enhancers")
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    #plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_%s_BAE_HISTO_normed_length_all" % (today, today, item))
    plt.close()
    ########## OVERLAID HISTOGRAM #########
    """
    data, bins, patches = plt.hist(data_a, bins, alpha = 0.5, normed = False,  label = ["all enh"])
    
    plt.legend()
    plt.xlabel("length (nt)")
    plt.xscale("log")
    plt.ylabel("Num Enhancers")
    plt.title("Length Of Villar Enhancers")

    
    plt.scatter(short_vrs["length"],short_vrs["sum"], width, alpha = 0.75, label = "All Enh")
    plt.scatter(short_vrs_90["length"], short_vrs_90["sum"], width, alpha = 0.75, label = "90th Enh")
    """
    
    ####### BOXPLOT #######
    plt.figure(num=None, figsize=(10,6), dpi=200, facecolor='w', edgecolor='k')
    #plt.boxplot(data_2, labels = labels2)
    #plt.boxplot(data_a, labels = labels)
    #plt.boxplot(data_h, labels = labels_h
    plt.boxplot(data_h, labels = labels_h)
    plt.yscale("log")
    
    plt.ylabel("Length of Enhancers (nt)")
    
    plt.xlabel("Percentile Of Tissue Overlap - %s in Roadmap" % x)
    plt.title("Overlap in Roadmap v. Length of Enhancers")
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    plt.savefig("/Users/sarahfong/Desktop/CAPRA/%s/%s_%s_BAE_length_con_only" % (today, today, item))
    plt.close()
    a,p = stats.ttest_ind(data_a[1], data_a[5])
    print(item, a, p)
    af, ap= stats.f_oneway(data_a[1], data_a[2], data_a[3], data_a[4], data_a[5])
    #cf, cp= stats.f_oneway(data_c[1], data_c[2], data_c[3], data_c[4], data_c[5])
    #hf, hp=stats.f_oneway(data_h[1], data_h[2], data_h[3], data_h[4], data_h[5])
    #print(item, "all, cons, hu", ap, cp, hp)
