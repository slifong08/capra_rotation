# My project's README

Method 1 = Attempt to identify broadly active enhancers by multi-intersecting all roadmap datasets together. The problem with this method is that fragments get shredded. https://www.evernote.com/l/AkD13BFqIcNDf6ItA5oP3YmYBKintwBOLd0

Method 2 - Villar-based approach to identifying broadly active enhancers takes the villar human liver enhancer fragments, queries those fragments for overlap each other roadmap tissue, then does logic to identify the amount of overlap. 

Basic Method to identify broadly active enhancers

1. identify enhancers based on H3K4AC27+ H3K4ME3- histone marks using ChIP-Seq datasets from roadmap encode. 
	/home/fongsl/broadly_active_enhancers/Make_roadmap_bedfiles.ipynb 
2. Use method 1 or 2 to identify active enhancers found both in Villar and roadmap
3. Identify broadly active enhancers by arbitrarly picking the 90th% of overlapping ChIP-seq fragments. 

For Conservation, sequence alignment and activity in different species - 
4. Only method2 was taken forward in /home/fongsl/capra_rotation/method2_v4.py. 
5. results were graphed using 
	vrs_bar_counts_v1.py 
	vrs_length_box.py
	vrs_length_line_dist.py