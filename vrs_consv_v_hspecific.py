# this script is to evaluate all of the villar x roadmap x species sequence alignments without having to run the entire method2_v1,2,3 scripts

import os, sys
import pandas
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
plt.style.use('seaborn-deep')
today = datetime.date.today()

# This dataframe is called "short_vrs" in the method2_v3.py script

vrs = pandas.read_csv("/Users/sarahfong/Desktop/CAPRA/2018-02-24/Villar_Roadmap_Species_short_vrs_overlap_2018-02-24.csv", sep = '\t')
vrs = pandas.read_csv("/Users/sarahfong/Desktop/CAPRA/2018-02-27/short_vrs_overlap_2018-02-27.csv", sep = '\t')

q25= int(vrs["sum"].quantile(0.25))
q50 = int(vrs["sum"].quantile(0.50))
q75 = int(vrs["sum"].quantile(0.75))
q90 = int(vrs["sum"].quantile(0.90))
 # 'a' = 'all'
 # 'h' = 'human' specific enhancers
 # 'c' = 'conserved' enhancers
 # 'd' = 'dataset'

 #'act' = active
 #'aln' = alignable

dtype = ['sum_act_sp', 'sum_aln_sp', 'sum_hq_act_sp', 'sum_hq_aln_sp']

vrs_0 = vrs.loc[vrs["sum"]<q25] # sum = the number of overlapping roadmap tissues
vrs_0a = vrs_0["length"]


vrs_25 = vrs.loc[vrs["sum"]>=q25]
vrs_25 = vrs_25.loc[vrs_25["sum"]<q50]

vrs_50 = vrs.loc[vrs["sum"]>=q50]
vrs_50 = vrs_50.loc[vrs_50["sum"]<q75]

vrs_75 = vrs.loc[vrs["sum"]>=q75]
vrs_75 = vrs_75.loc[vrs_75["sum"]<q90]

vrs_90 = vrs.loc[vrs["sum"]>=q90]
       
for item in dtype:
    hspec = vrs_90[["v-chr", "v-start", "v-end"]].loc[vrs_90['%s' %item] == 0]
    hspec.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/hspec_90_%s_zero_%s.bed" %(today, item, today), sep = '\t', index = False, header = False)

    consv = vrs_90[["v-chr", "v-start", "v-end"]].loc[vrs_90['%s'%item] > 0]
    consv.to_csv("/Users/sarahfong/Desktop/CAPRA/%s/consv_vrs_90_%s_greater_than_zero_%s.bed" %(today ,item, today), sep = '\t', index = False, header = False)
    
